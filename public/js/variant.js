$(function() {
	$('#add-to-cart').on('click', function() {
		$.post('/api/add-to-cart',
			{ 
				product_id: $('#product-id').data('productid'),
				variant_id: $('#variant-id').data('variantid') 
			}, function() {
				window.location = '/cart';
			});
	});
});
