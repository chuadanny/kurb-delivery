// Show alert box using Javascript
// function showAlert(msg) {
// 	$('#alert-msg')
// 		.text(msg)
// 		.removeClass('in')
// 		.removeClass('hidden')
// 		.addClass("in")
// 		.alert();
// 	window.setTimeout(function() { 
// 		$(".alert").removeClass('in').addClass('hidden'); 
// 	}, 3000);
// }





// Show alert box using Javascript
function showAlert(msg) {
	$('#alert-msg')
		.text(msg)
		.fadeIn()
		.removeClass('hidden');
	window.setTimeout(function() { 
		$(".alert").fadeOut(); 
	}, 3000);
}



$(function() {

/* ==========================================================================
   Common
   ========================================================================== */
// Dropdown menu: Fade + Slide
$('.dropdown-toggle').click(function() {
    if($(this).siblings('.dropdown-menu').css('display') == 'none') {
        $(this).siblings('.dropdown-menu')
        	.stop(true, true)
		    .animate({
		        height:"toggle",
		        opacity:"toggle"
		    },300);
    } else {
    	$(this).siblings('.dropdown-menu').fadeOut(50);
    }
});



// // Show Alert if msg is stored in PHP session
// if($('#alert-msg').text()) {
// 	showAlert( $('#alert-msg').text() );
// }




// // Fades .fade elements in (with accompanying CSS)
// $(".fade").addClass("in");

// // Allows alerts to be closed using JS
// $(".alert").alert();

// // Fades alerts out automatically
// window.setTimeout(function() { $(".alert").alert('close'); }, 3000);











/* ==========================================================================
   Products / Add Product
   ========================================================================== */

	$('#product-add-form-submit').click(function(e) {

		$('#product-add-form').submit();

	});


		$('#product-add-form').validate({
			rules: {
				'product-name': {
					required: true
				},
				'product-price': {
					required: true
				},
				'product-retail-price': {
					required: false
				},
				'product-sku': {
					required: false
				},
				'product-size': {
					required: true
				},
				'product-color': {
					required: false
				},
				'product-condition': {
					required: true
				},
				'product-photo': {
					required: true
				},
			},
			errorClass: 'alert',
			highlight: function(element, errorClass) {
				// Blink
			    $(element).fadeOut(function() {
			      $(element).fadeIn();
			    });
			    // Add class
			    $(element).addClass('validation-error');
			  },
			  unhighlight: function(element) {
			  	// Remove error class
			  	$(element).removeClass('validation-error');
			  }
		});





/* ==========================================================================
   Products / Edit Product
   ========================================================================== */

   	// Form Submission
	$('.submit-form-btn').click(function(e) {

		$('#product-edit-form').submit();
	});


	// Form Validation
	$('#product-edit-form').validate({
		rules: {
			'product-name': {
				required: true
			},
			'product-price': {
				required: true
			},
			'product-retail-price': {
				required: true
			},
			'product-sku': {
				required: true
			},
			'product-size': {
				required: true
			},
			'product-color': {
				required: true
			},
			'product-condition': {
				required: true
			},
			'product-photo': {
			},
		},
		errorClass: 'alert',
		highlight: function(element, errorClass) {
			// Blink
		    $(element).fadeOut(function() {
		      $(element).fadeIn();
		    });
		    // Add class
		    $(element).addClass('validation-error');
		  },
		  unhighlight: function(element) {
		  	// Remove error class
		  	$(element).removeClass('validation-error');
		  }
	});


	// Show Delete Modal
	$('#product-delete').on('click', function() {
		$('#product-delete-modal').modal();
	});

	// Delete Product Modal
	$('#product-delete-modal #confirm-product-delete').on('click', function() {
		$.delete_('/api/v1/variants/' + $('#variant-id').val(),
			function() {
				window.location.href = '/admin/products';
			});
	});




	///////////////
	//  Filters  //
	///////////////

	
	// Prevent dropdown menu from closing when selecting filters
	//  - But allows click to hit Filter's apply btn
	$('.search-box .dropdown-menu').on('click', function(e) {
		if(!$(e.target).is('#filters-apply-btn'))
			e.stopPropagation();
	});


	// 'Apply Filter' btn submits form
	$('.search-box .dropdown-menu #filters-apply-btn').on('click', function() {
		$('#filter-form').submit();
	});










/* ==========================================================================
   Orders
   ========================================================================== */

   	// Label styles
	$('#orders-table .label').each(function(index) {
		console.log( $(this).text() );

		// Fulfillments
		if( $(this).text() == 'Pending' )	$(this).addClass('label-warning');
		if( $(this).text() == 'Partial' )	$(this).addClass('label-warning');
		if( $(this).text() == 'Fulfilled' )	$(this).addClass('label-success');

		// Payment
		if( $(this).text() == 'Paid' )	$(this).addClass('label-success');
		if( $(this).text() == 'Refunded' )	$(this).addClass('label-info');
   	});





/* ==========================================================================
   Order
   ========================================================================== */

   	// // Toggle checkbox's cell's highlighting
   	// $('.order-contents .checkbox input[type=checkbox]').on('click', function() {
   	// 	// Toggle checkbox's cell's highlighting
   	// 	if($(this).is(':checked')) {
   	// 		$(this).parent().addClass('is-selected');
   	// 	} else {
   	// 		$(this).parent().removeClass('is-selected');
   	// 	}

   	// 	// Update total line items checked
   	// 	var numItemsChecked = $('.order-contents').find('input:checked').length;
   	// 	if(numItemsChecked > 1) {
   	// 		var noun = 'line items';
   	// 	} else {
   	// 		var noun = 'line item';
   	// 	}

   	// 	$('#line-items-checked').text('Fulfill ' + numItemsChecked + ' ' + noun);
   	// });



	// Fulfill entire order: Step 1
	$('#fulfill-order-btn').on('click', function() {
		$('#fulfill-modal').modal();
	});



	// Fulfill entire order: Step 2
	$('#fulfill-modal #submit-btn').on('click', function() {

		var orderId = $('#order-id').val();

		$.put('/api/v1/orders/' + orderId, {

			completeFulfillment: 1,
			trackingNumber: $('#tracking-number').val()

		}, function() {

			$('#fulfill-modal').on('hide', function() {
				showAlert('Your order has been fulfilled.');
			});
			$('#fulfill-modal').modal('hide');

			// Disable the button
			$('#fulfill-order-btn')
				.addClass('disabled')
				.removeClass('btn-primary')
				.text('All line items fulfilled');

		});
	});



   	// Save changes to order 
   	$('#order-update-btn').on('click', function() {

		var orderId = $('#order-id').val();

   		$.put('/api/v1/orders/' + orderId,
   			{
   				orderId: $('#order-id').val(),
   				notes: $('#order-notes').val()
   			}, function() {
   				showAlert('Your note has been saved.');
   			});

   	});










 /* ==========================================================================
   Customer
   ========================================================================== */
   	$('#customer-update-btn').on('click', function() {

   		$.put('/api/v1/customers/update/' +  $('#customer-id').val(),
	   		{
	   			notes: $('#customer-notes').val()
	   		}, function() {
   				showAlert('Your note has been saved.');
	   		});
   	});

});