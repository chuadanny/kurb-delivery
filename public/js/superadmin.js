$(function() {

/* ==========================================================================
   	Dashboard
   ========================================================================== */
$('#all-variants-pending-btn').on('click', function() {
	$.post('/superadmin-api/all-variants-pending', 
		function() {
			location.reload();
		});

});











/* ==========================================================================
   	Approvals
   ========================================================================== */

$('#approve-btn').on('click', function(e) {

	e.preventDefault();

	$.post('/superadmin-api/approval-variant', 
		{
			'variant-id': $('#variant-id').val(),
			'approve': 1,
			'label-hot': $('#label-hot').prop('checked'),
		}, function() {
			location.reload();
		});

});


$('#reject-btn').on('click', function(e) {
	e.preventDefault();
	$('#reject-modal').modal();
});


$('#reject-modal #submit-reject').on('click', function() {

	$.post('/superadmin-api/approval-variant', 
		{
			'variant-id': $('#variant-id').val(),
			'approve': 0,
			'reason': $('#reject-reason').val(),
			'optional-reason': $('#optional-reason').val(),
		}, function() {
			location.reload();
		});

});


$('#edit-btn').on('click', function(e) {
	e.preventDefault();
	$('#edit-modal').modal();
});


$('#edit-modal #submit-edited-product').on('click', function(e) {
	e.preventDefault();
    $(this).button('loading');

	// HTML5 AJAX File upload
	$.ajax({
	    url: '/superadmin-api/edit-variant', //server script to process data
	    type: 'POST',
	    data: new FormData($('#edit-product-form')[0]),
	    success: function(result) {
	        console.log($.ajaxSettings.xhr().upload);
	        location.reload();
	    },

	    //Options to tell JQuery not to process data or worry about content-type
	    cache: false,
	    contentType: false,
	    processData: false
	});	
	
});
























});