$(function($) {

	// Validation
	jQuery.validator.addMethod("notEqual", function(value, element, param) {
		return this.optional(element) || value !== param;
	}, "Please choose a value!");

	$('#payment-form').validate({
		rules: {
			first_name: "required",
			last_name: "required",
			email: {
				required: true,
				email: true
			},
			phone: {
				required: true,
				phoneUS: true
			},

			address_1: 'required',
			city: 'required',
			state: {
				required: true,
				notEqual: '',
			},
			postal: {
				required: true,
				zipcodeUS: true
			},

		},

		messages: {
			first_name: 'Your first name is required.',
			last_name: 'Your last name is required.',
			email: {
				required: 'Your email address is required.'
			},
			phone: {
				required: 'Your phone number is required.'
			},
			address_1: 'Your address is required.',
			city: 'Your city is required.',
			state: {
				required: 'Your state is required.',
				notEqual: 'Your state is required.'
			},
			postal: {
				required: 'Your postal code is required.'
			}
		},

		errorPlacement: function(error, element) {
		    if (element.attr("name") == "first_name" || element.attr("name") == "last_name" ) {
		      error.insertAfter("input[name='last_name']");
		    } else if (element.attr("name") == "address_1" || element.attr("name") == "address_2" 
		    	|| element.attr("name") == "city" || element.attr("name") == "state"
		    	|| element.attr("name") == "postal") {
		      error.insertAfter("input[name='postal']");
		    } else {
		      error.insertAfter(element);
		    }
		},

		errorElement: 'label',
		errorClass: 'alert',

		highlight: function(element) {
			// Dont apply errorClass to element
			return false;
		},

		unhighlight: function(element) {
			// Dont remove errorClass from element
			return false;
		},

		submitHandler: function(event) {
			// Pass control to Stripe form handler
			$(this).submit();
		}

	});


	// Stripe form handler
	$('#payment-form').submit(function(event) {
		var $form = $(this);

		// Disable the submit button to prevent repeated clicks
		$form.find('button').prop('disabled', true);

		Stripe.createToken($form, stripeResponseHandler);

		// Prevent the form from submitting with the default action
		return false;	
	});


  var stripeResponseHandler = function(status, response) {
	  var $form = $('#payment-form');

	  if (response.error) {
	    // Show the errors on the form
	    $form.find('.payment-errors')
	    	.text(response.error.message)
	    	.fadeIn();
	    $form.find('button').prop('disabled', false);
	  } else {
	    // token contains id, last4, and card type
	    var token = response.id;

	    // Insert the token into the form so it gets submitted to the server
	    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
	    
	    // and submit
	    $form.get(0).submit();
	  }
	};

});