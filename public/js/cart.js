$(function() {
	$('.remove-from-cart').on('click', function() {
		$.post('/api/remove-from-cart',
			{ rowid: $(this).data('rowid') },
			function() {
				location.reload();
			});
	});
});
