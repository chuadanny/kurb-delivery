// Show alert box using Javascript
function showAlert(msg) {
	$('#alert-msg')
		.fadeIn()
		.removeClass('hidden');
	window.setTimeout(function() { 
		$(".alert").fadeOut(); 
	}, 3000);
}
