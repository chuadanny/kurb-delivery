<?php

class api_VariantsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// Get Variant
		$variant = Variant::find($id);

		// Ensure that Variant is owned by User & Not out of stock
		if(Auth::user()->shop->id == $variant->shop->id
			&& $variant->quantity > 0) {

			// Delete Photos
			foreach($variant->photos as $photo) {
				$photo->removeAllFiles();	// delete image files
				$photo->delete();			// delete Model
			}

			// Delete Variant
			$variant->delete();

		} else {

			// Someone is trying to hack
			// TODO: Log attempt

		}

	}

}