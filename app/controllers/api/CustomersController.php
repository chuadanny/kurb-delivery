<?php

class api_CustomersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		// Get user's shop
		$shop = Auth::user()->shop;

		// Get Customer
		$customer = Customer::find($id);

		// Handle CustomerNotes only if submitted PUT request has the variable
		$inputNotes = Input::get('notes');	// Updating to PHP 5.5 avoids this extra step
		if(isset($inputNotes)) {

			// Update changes
			if(Input::get('notes')) {

				if(!$customer->customerNotes()->count()) {
					// Create new CustomerNotes if does not exist
					$notes = new CustomerNotes([
						'notes' => Input::get('notes'),
						'shop_id' => $shop->id
					]);
				} else {
					// Else update CustomerNotes
					$notes = $customer->customerNotes()->first();
					$notes->notes = Input::get('notes');
				}

				// Execute update of CustomerNotes
				$customer->customerNotes()
					->where('shop_id', $shop->id)
					->save($notes);
			} else {
				// Delete the note if it exist
				if($customer->customerNotes()->count()) {
					$customer->customerNotes()
						->where('shop_id', $shop->id)
						->delete();
				}
			}
		}

		return Response::json([
			'error' => false,
			'message' => 'updated'
			], 200);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}