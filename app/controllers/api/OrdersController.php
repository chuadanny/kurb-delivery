<?php

class api_OrdersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
				
		// Get Order
		$order = Order::find(Input::get('orderId'));

		// Update notes
		$orderNotes = Input::get('notes');	// Avoid this step by updating to PHP5.5
		if(isset($orderNotes)) {
			$order->notes = Input::get('notes');
		}

		if(Input::get('completeFulfillment')) {
			foreach($order->orderVariants as $orderVariant) {
				$orderVariant->fulfillment = 'fulfilled';
				$orderVariant->tracking_number = Input::get('trackingNumber');
				$orderVariant->update();
			}
		}

		// Execute update
		$order->update();


		return Response::json([
			'error' => false,
			'message' => 'updated'
			], 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}