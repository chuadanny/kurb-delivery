<?php

class SuperadminApiController extends BaseController {


	public function postApprovalVariant() {

		// Update Variant
		$variant = Variant::find(Input::get('variant-id'));
		$variant->is_approved = Input::get('approve');
		$variant->label_hot = (int) (Input::get('label-hot') === 'true') ;
		$variant->save();


		// Update Approval
		$approval = $variant->approval;

		if(Input::get('approve') == 1) {
			$approval->status = 'approved';
		} else {
			$approval->status = 'rejected';
			$approval->reason = Input::get('reason');
			$approval->optional_reason = Input::get('optional-reason');
		}

		$approval->save();


		// TODO: Send notification email if rejected

	}


	public function postEditVariant() {

		// Update Variant
		$variant = Variant::find(Input::get('variant-id'));
		$variant->color_id = Input::get('product-color');
		$variant->save();

		// Update Product
		$product = Product::find($variant->product->id);
		$product->category_id = Input::get('product-category');
		$product->gender_id = Input::get('product-gender');
		$product->style_id = Input::get('product-style');
		$product->material_id = Input::get('product-material');
		$product->save();

		// Upload photo
		if(Input::file('product-photo')) {
			$src = Input::file('product-photo')->getRealPath();
			$photo = Photo::find($variant->photos()->first()->id);

			$img_server = $photo->img_server;
			$img_root = $photo->img_root;
			$img_bucket = $photo->img_bucket;
			$img_subpath = $photo->img_subpath;
			$img_filename_original 	= $photo->img_filename_original;
			$img_filename_xlarge 	= $photo->img_filename_xlarge;
			$img_filename_large 	= $photo->img_filename_large;
			$img_filename_medium 	= $photo->img_filename_medium;
			$img_filename_small 	= $photo->img_filename_small;

			// Overwrite original photo
			Image::copyOriginal($src, $img_root . $img_bucket . $img_subpath . $img_filename_original);

			// Overwrite the different sizes
			Image::resizeXlarge($img_root . $img_bucket . $img_subpath . $img_filename_original, $img_root . $img_bucket . $img_subpath . $img_filename_xlarge);
			Image::resizeLarge($img_root . $img_bucket . $img_subpath . $img_filename_original, $img_root . $img_bucket . $img_subpath . $img_filename_large);
			Image::resizeMedium($src, $img_root . $img_bucket . $img_subpath . $img_filename_medium);
			Image::resizeSmall($src, $img_root . $img_bucket . $img_subpath . $img_filename_small);
		}
		
	}


	public function postAllVariantsPending() {
		foreach(Approval::all() as $approval) {
			// Update Approval
			$approval->status = 'pending';
			$approval->save();

			// Update Variant
			$variant = $approval->variant;
			$variant->is_approved = 0;
			$variant->label_hot = 0;
			$variant->save();
		}
	}

}