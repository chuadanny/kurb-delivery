<?php

class ShopController extends BaseController {


	/**
	 * Display Splash page
	 * @return void
	 */
	public function getIndex() {

		$shops = Shop::where('is_active', 1)->get();

		return View::make('shops.main')
					->with('shops', $shops);
	}


	public function getView($id) {

		$shop = Shop::find($id);
		$variants = $shop->variants;

		return View::make('shops.shop')
					->with('shop', $shop)
					->with('variants', $variants);

	}










}
