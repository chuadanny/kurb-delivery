<?php

class ApiController extends BaseController {


	public function postAddToCart() {

		$product_id = Input::get('product_id');
		$variant_id = Input::get('variant_id');
		$name = Product::find($product_id)->name;
		$qty = 1;
		$price = Variant::find($variant_id)->price;

		Cart::add($variant_id, $name, $qty, $price);
	}

	public function postRemoveFromCart() {

		$rowid = Input::get('rowid');

		Cart::remove($rowid);
	}

}