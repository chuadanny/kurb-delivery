<?php

class SuperadminController extends BaseController {

	public function __construct() {
    	$this->beforeFilter('superadminAuth');
    }


	public function getIndex() {

		return View::make('superadmin.index')
				->with('currentPage', 'home');
	}

	public function getApprovals() {

		// Find next Variant to approve
		$approval = Approval::orderBy('id', 'desc')
						->where('status', 'pending')
						->first();
						
		if(!$approval)
			return View::make('superadmin.msg')
					->with('currentPage', 'approvals')
					->with('msg', 'Nothing to approve!');

		$variant = $approval->variant;
		$photo = $variant->photos()->first();
		$shop = $variant->shop;


		// Get Photo's Metadata
		$reader = \PHPExif\Reader::factory(\PHPExif\Reader::TYPE_NATIVE);
		$exif = $reader->getExifFromFile(Image::getFullPath($photo));


		return View::make('superadmin.approvals')
				->with('currentPage', 'approvals')
				->with('variant', $variant)
				->with('photo', $photo)
				->with('shop', $shop)
				->with('exif', $exif);
	}











}

