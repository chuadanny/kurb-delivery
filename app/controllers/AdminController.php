<?php

class AdminController extends BaseController {

	public function __construct() {
    	$this->beforeFilter('adminAuth');
    }


	public function getIndex() {

		// Most Recent Orders
		//  - Retrieve shop's product variants
		//  - Order's relationships preserved
		$orders = OrderVariant::join('transactions', 'order_variant.order_id', '=', 'transactions.order_id')
							->join('orders', 'order_variant.order_id', '=', 'orders.id');

		// Restrict to user's shop
		$shop = Shop::where('user_id', Auth::user()->id)->first();
		$orders = $orders
					->where('order_variant.shop_id', $shop->id)
					->where('order_variant.fulfillment', 'pending');


		// Execute Query
		$orders = $orders->get();

		// Create an array of Order objects
		$orders_array = [];
		foreach($orders as $order) {
			$orders_array[$order->id] = Order::find($order->id);
		}

		// Basically doing a LIMIT() query without using duplicates from JOIN
   		$orders_array = array_slice( array_reverse($orders_array, true), 0, 5, true);



		return View::make('admin.index')
			->with('currentPage', 'home')
			->with('orders', $orders_array)
			->with('shop', $shop);
	}










/* ==========================================================================
   Products
   ========================================================================== */
	public function getProducts() {

		// Settings
		$perPage = 10;

		// Retrieve shop's product variants
		$shop = Shop::where('user_id', Auth::user()->id)->first();
		$products = Product::join('variants', 'products.id', '=', 'variants.product_id')
						->where('variants.shop_id', $shop->id);


		// Process Filters (Variant)
		if(Input::get('filter-visibility') > -1)	
			$products = $products->where('variants.is_visible', Input::get('filter-visibility'));

		// Process Filters (Product)
		if(Input::get('filter-brand'))		$products->where('products.brand_id', Input::get('filter-brand'));
		if(Input::get('filter-category'))	$products->where('products.category_id', Input::get('filter-category'));
		if(Input::get('query'))				$products->where('products.name', 'like', '%'.Input::get('query').'%');
			
		// Execute Query
		$products = $products->get();

		// Return if no results
		if(!count($products)) {
			return View::make('admin.products')
				->with('currentPage', 'products')
				->with('query', Input::get('query'))
				->with('oldInput', Input::except('page'));	// persists GET parameters
		}

		// Gather variants' IDs
		$variant_ids = [];
		foreach($products as $product) {
			$variant_ids[] = $product->id;	// $product->id becomes variants.id after the join
		}

		// Retrieve the filtered Variants
		$filteredVariants = Variant::find($variant_ids);


		// Convert to an array of Objects
		$filteredVariantsArray = [];
		foreach($filteredVariants as $variant) {
			$filteredVariantsArray[] = $variant;
		}

		// Paginate
		$variants = Paginator::make($filteredVariantsArray, count($filteredVariants), $perPage);

		return View::make('admin.products')
			->with('currentPage', 'products')
			->with('variants', $variants)
			->with('query', Input::get('query'))
			->with('oldInput', Input::except('page'));	// persists GET parameters

	}








// 	public function getProductsTest() {
// 		// Settings
// 		$perPage = 10;

// 		// Retrieve shop's product variants
// 		$shop = Shop::where('user_id', Auth::user()->id)->first();
// 		$variants = Variant::where('shop_id', $shop->id);

// 		// Process Filters (Variant)
// 		if(Input::get('filter-visibility'))	
// 			$variants = $variants->where('is_visible', Input::get('filter-visibility'));

// 		// Process Filters (Product)
// 		$queryString = Input::get('query');
// 		$variants = $variants->with(array('product' => function($query) use($queryString) {
// 			if(Input::get('filter-brand'))			$query->brand(Input::get('filter-brand'));
// 			if(Input::get('filter-category'))		$query->category(Input::get('filter-category'));
// 			if(Input::get('filter-query'))			$query->brand(Input::get('filter-query'));
// 		}));
		
// 		// $variants = $variants->products(Input::get());

			
// 		// Execute Query
// 		$variants = $variants->get();

// 		// Filter variants whose product's name matches query string (Part 2)
// 		//  - Variants whose related product do not satisfy eager load constrains
// 		//    are returned too, so this filter will get rid of them
// 		//  - Added to an array so paginate() can use them
// 		//  - Array elements remain as Eloquent Objects so relationships are maintained
// 		//    and used in Blade templates (eg: {{ $variant->product->name }})

// 		$filteredVariants = [];		// an array of Eloquent objects
// 		foreach($variants as $variant) {
// 			if($variant->product)
// 				$filteredVariants[] = $variant;
// 		}
		
// 		// Paginate
// 		$variants = Paginator::make($filteredVariants, count($filteredVariants), $perPage);



// 		// echo '<pre>';
// 		// print_r(DB::getQueryLog());
		


// echo '<pre>';
// 		foreach($filteredVariants as $variant) {
// 			print_r($variant);
// 		}
// 	}









	public function getProductAdd() {

		// Remove 'Other' and append to the end.
		$brands = Brand::orderBy('name')->get();

		foreach($brands as $index => $brand) {
			if($brand['name'] == 'Other') {
				$other = $brand;
				unset($brands[$index]);
			}
		}
		$brands[] = $other;

		return View::make('admin.product-add')
			->with('currentPage', 'products')
			->with('brands', $brands);

	}


	public function postProductAdd() {

		// Note:
		// 	Remember to increase File upload size in php.ini!
		// 	`post_max_size = 10M`
		// 	Otherwise Input::file() returns '/var/www/kurb/public'

		// Product
		$productData = array(
			'name' 				=> Input::get('product-name'),
			'description' 		=> Input::get('product-description'),
			'category_id'		=> Input::get('product-category'),
			'brand_id' 			=> Input::get('product-brand'),
			'gender_id' 		=> Input::get('product-gender'),
			'price' 			=> Input::get('product-price'),
			'retail_price'		=> Input::get('product-retail-price'),
			'style_id'			=> Input::get('product-style'),
			'category_id'		=> Input::get('product-category'),
			'material_id'		=> Input::get('product-material'),
		);

		$product = Product::create($productData);


		// Variant
		$variantData = array(
			'product_id'		=> $product->id,
			'price' 			=> Input::get('product-price'),
			'retail_price'		=> Input::get('product-retail-price'),
			'sku' 				=> Input::get('product-sku'),
			'internal_sku'		=> Input::get('product-internal-sku'),
			'size_id' 			=> Input::get('product-size'),
			'color_id' 			=> Input::get('product-color'),
			'condition_id' 		=> Input::get('product-condition'),
			'weight' 			=> Input::get('product-weight'),
			'is_visible'		=> Input::get('product-visibility'),
			'is_approved'		=> '0',
			'shop_id'			=> Auth::User()->shop->id,
		);

		$variant = Variant::create($variantData);


		// Photo
		$img_server = 'www.kurb.com';
		$img_root = public_path();
		$img_bucket = '/img';
		$img_subpath = '/products/';
		$img_filename_original 	= $variant->id . '_original.jpg';
		$img_filename_xlarge 	= $variant->id . '_xlarge.jpg';
		$img_filename_large 	= $variant->id . '_large.jpg';
		$img_filename_medium 	= $variant->id . '_medium.jpg';
		$img_filename_small 	= $variant->id . '_small.jpg';

		// Image manipulation Operations
		

		// Make local copy
		$src = Input::file('product-photo')->getRealPath();
		$localCopy = $img_root . $img_bucket . $img_subpath . $img_filename_original;
		Image::copyOriginal($src, $localCopy);

		// Make resized copies
		Image::resizeXlarge($localCopy, $img_root . $img_bucket . $img_subpath . $img_filename_xlarge);
		Image::resizeLarge($localCopy, $img_root . $img_bucket . $img_subpath . $img_filename_large);
		Image::resizeMedium($localCopy, $img_root . $img_bucket . $img_subpath . $img_filename_medium);
		Image::resizeSmall($localCopy, $img_root . $img_bucket . $img_subpath . $img_filename_small);


		// Upload to S3
		$s3_img_server = 's3.amazonaws.com';
		$s3_img_root = '';
		$s3_img_bucket = '/static.kurb.com.production';
		$s3_img_subpath = '/products/';

		$s3 = AWS::get('s3');
		$s3->putObject(array(
		    'Bucket'	=> 'static.kurb.com.production',
		    'Key'       => $s3_img_subpath . $img_filename_original,
		    'SourceFile' => $img_root . $img_bucket . $img_subpath . $img_filename_original,
    		'ACL'    	=> 'public-read',
		));
		$s3->putObject(array(
		    'Bucket'	=> 'static.kurb.com.production',
		    'Key'		=> $s3_img_subpath . $img_filename_xlarge,
		    'SourceFile'=> $img_root . $img_bucket . $img_subpath . $img_filename_xlarge,
    		'ACL'		=> 'public-read',
		));
		$s3->putObject(array(
		    'Bucket'	=> 'static.kurb.com.production',
		    'Key'		=> $s3_img_subpath . $img_filename_large,
		    'SourceFile'=> $img_root . $img_bucket . $img_subpath . $img_filename_large,
    		'ACL'		=> 'public-read',
		));
		$s3->putObject(array(
		    'Bucket'	=> 'static.kurb.com.production',
		    'Key'		=> $s3_img_subpath . $img_filename_medium,
		    'SourceFile'=> $img_root . $img_bucket . $img_subpath . $img_filename_medium,
    		'ACL'		=> 'public-read',
		));
		$s3->putObject(array(
		    'Bucket'	=> 'static.kurb.com.production',
		    'Key'		=> $s3_img_subpath . $img_filename_small,
		    'SourceFile'=> $img_root . $img_bucket . $img_subpath . $img_filename_small,
    		'ACL'		=> 'public-read',
		));


		// // Remove temp files from server
		// $tmpFiles = [
		// 	$img_root . $img_bucket . $img_subpath . $img_filename_original,
		// 	$img_root . $img_bucket . $img_subpath . $img_filename_xlarge,
		// 	$img_root . $img_bucket . $img_subpath . $img_filename_large,
		// 	$img_root . $img_bucket . $img_subpath . $img_filename_medium,
		// 	$img_root . $img_bucket . $img_subpath . $img_filename_small
		// ];
		// foreach ($tmpFiles as $tmpFile) {
		// 	if (file_exists($tmpFile))
		// 		unlink ($tmpFile);
		// }


		// Store in database
		$photoData = array(
			'product_id'	=> $product->id,
			'variant_id'	=> $variant->id,
			'img_root' 		=> $s3_img_root,
			'img_server'	=> $s3_img_server,
			'img_bucket'	=> $s3_img_bucket,
			'img_subpath'	=> $s3_img_subpath,
			'img_filename_original'	=> $img_filename_original,
			'img_filename_xlarge'	=> $img_filename_xlarge,
			'img_filename_large'	=> $img_filename_large,
			'img_filename_medium'	=> $img_filename_medium,
			'img_filename_small'	=> $img_filename_small,
		);

		Photo::create($photoData);


		// Queue photos for Approval
		$approvalData = array(
			'status'		=> 'pending',
			'variant_id'	=> $variant->id
		);

		Approval::create($approvalData);


		return Redirect::to('/admin/products')
			->with('currentPage', 'products');
	}





	public function getVariant($id) {
		$variant = Variant::find($id);

   		// Determine View template
   		if($variant->quantity) {
   			$template = 'admin.product-edit';
   		} else {
   			$template = 'admin.product-readonly';
   		}

		return View::make($template)
					->with('variant', $variant)
					->with('product', $variant->product)
					->with('currentPage', 'products');	// Convenience
	}


	public function postVariantEdit() {

		// Variant
		$variantData = array(
			'price' 			=> Input::get('product-price'),
			'retail_price'		=> Input::get('product-retail-price'),
			'sku' 				=> Input::get('product-sku'),
			'internal_sku' 		=> Input::get('product-internal-sku'),
			'size_id' 			=> Input::get('product-size'),
			'color_id' 			=> Input::get('product-color'),
			'condition_id' 		=> Input::get('product-condition'),
			'weight' 			=> Input::get('product-weight'),
			'is_visible'		=> Input::get('product-visibility'),
			'is_approved'		=> '0',
			'shop_id'			=> Auth::User()->shop->id,
		);

		$variant = Variant::find(Input::get('variant-id'));

		// Save update
		if($variant->quantity > 0) {
			$variant->update($variantData);
		} else {
			// Prevent hack attempt
			// TODO: Log attempt
			return Redirect::to('/admin/variant/' . $variant->id)
				->with('currentPage', 'products');
		}

		// Product
		$productData = array(
			'name' 				=> Input::get('product-name'),
			'description' 		=> Input::get('product-description'),
			'category_id'		=> Input::get('product-category'),
			'brand_id' 			=> Input::get('product-brand'),
			'gender_id' 		=> Input::get('product-gender'),
			'price' 			=> Input::get('product-price'),
			'retail_price'		=> Input::get('product-retail-price'),
		);

		$product = Product::find($variant->product->id);
		$product->update($productData);


		return Redirect::to('/admin/variant/' . $variant->id)
				->with('msg', 'Product was successfully saved.')
				->with('currentPage', 'products');
	}











/* ==========================================================================
   Orders
   ========================================================================== */
   	public function getOrders() {
   		$perPage = 10;

		// Retrieve shop's product variants
		//  - Order's relationships preserved
		$orders = OrderVariant::join('transactions', 'order_variant.order_id', '=', 'transactions.order_id')
							->join('orders', 'order_variant.order_id', '=', 'orders.id');

		// Restrict to user's shop
		$shop = Shop::where('user_id', Auth::user()->id)->first();
		$orders = $orders->where('order_variant.shop_id', $shop->id);

		// Filters
		//  - Order matters, query filter should be last
		if(Input::get('filter-payment'))
			$orders = $orders->where('transactions.status', Input::get('filter-payment'));

		if(Input::get('filter-fulfillment'))
			$orders = $orders->where('order_variant.fulfillment', Input::get('filter-fulfillment'));

		if(Input::get('query'))
			$orders = $orders
						->where('orders.id', Input::get('query'))
						->orWhere('orders.first_name', 'like', '%' . Input::get('query') . '%')
						->orWhere('orders.last_name', 'like', '%' . Input::get('query') . '%')
						->orWhere(DB::raw('CONCAT(orders.first_name, " ", orders.last_name)'), 'like', '%' . Input::get('query') . '%');

		// Execute query
		$orders = $orders->get();


		// Create an array of Order objects
		$orders_array = [];
		foreach($orders as $order) {
			$orders_array[$order->id] = Order::find($order->id);
		}

		// Pagination
   		$paginated_orders = Paginator::make($orders_array, count($orders_array), $perPage);


   		return View::make('admin.orders')
			->with('currentPage', 'orders')
   			->with('orders', $paginated_orders);

   	}



   	public function getOrder($id) {
   		$order = Order::find($id);

   		return View::make('admin.order')
   					->with('currentPage', 'orders')
   					->with('order', $order);
   	}










/* ==========================================================================
   Customers
   ========================================================================== */

   	public function getCustomers() {
   		$perPage = 10;

		// Restrict to user's shop
		$shop = Shop::where('user_id', Auth::user()->id)->first();

		// Orders belonging to shop
   		$orders = OrderVariant::join('orders', 'orders.id', '=', 'order_variant.order_id')
   						->where('order_variant.shop_id', $shop->id)
   						->get();

   		// Customers whose orders include items from shop
   		$customers = [];
   		foreach($orders as $order) {
   			$order = Order::find($order->id);	// Retrieve again to maintain relationships
   			$customers[$order->customer->id] = $order->customer;	// Group duplicates + Form array for paginator
   		}

   		$pagianted_customers = Paginator::make($customers, count($customers), $perPage);

   		return View::make('admin.customers')
   				->with('currentPage', 'customers')
   				->with('customers', $pagianted_customers)
   				->with('shop', $shop);
   	}


   	public function getCustomer($customerId) {
   		$perPage = 10;

		// Current user's Shop
		$shop = Shop::where('user_id', Auth::user()->id)->first();
		
		// Customer Notes
		$notes = CustomerNotes::where('customer_id', $customerId)
					->where('shop_id', $shop->id)
					->first();
		if($notes)
			$notes = $notes->notes;

		// Customer
   		$customer = Customer::find($customerId);

   		// Customer orders from this shop
   		$orders = $customer->orders;
   		$orders_array = [];
   		foreach($orders as $i => $order) {
   			$orders_array[$order->id] = $order;
   		}

   		$paginated_orders = Paginator::make($orders_array, count($orders_array), $perPage);

   		return View::make('admin.customer')
   				->with('currentPage', 'customers')
   				->with('customer', $customer)
   				->with('orders', $paginated_orders)
   				->with('notes', $notes)
   				->with('shop', $shop);
   	}









/* ==========================================================================
   Login
   ========================================================================== */

	/**
	 * Login Page
	 * @return void
	 */
	public function getLogin() {
		return View::make('admin.login');
	}


	/**
	 * Process Login
	 * @return void 
	 */
	public function postLogin() {
		$credentials = array (
			'email' => Input::get('email'), 
			'password' => Input::get('password'),
			'active' => 1
		);

		if(Auth::attempt($credentials)) {
			return Redirect::to('/admin');
		} else {
			return Redirect::to('/admin/login')
						->with('error', 'Incorrect email/password');
		}
	}


	/**
	 * Logins as a particular user
	 * @param  int $id User ID
	 * @return void     
	 */
	public function getLoginAs($id) {
		Auth::loginUsingId($id);

		return Redirect::to('/admin');
	}


	/**
	 * Logs out a particular user
	 * @return void
	 */
	public function getLogout() {
		Auth::logout();

		return Redirect::to('/admin');
	}
}