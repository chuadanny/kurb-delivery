<?php

class StoreController extends BaseController {


	/**
	 * Display Splash page
	 * @return void
	 */
	public function getIndex() {
		return Redirect::to('http://launch.kurb.com');
		// return Redirect::action('StoreController@getSplash');
	}



	/**
	 * Display Splash page
	 * @return void
	 */
	public function getSplash() {
		return View::make('store.splash');
	}





	/**
	 * Browse through all Product Variants
	 * @return void
	 */
	public function getBrowse() {

	    // Join tables
	    $variants = Variant::join('products', 'products.id', '=', 'variants.product_id')
	    					->select('variants.id');

	    // Filters
	    if(Input::has('gender'))        $variants = $variants->whereIn('products.gender_id', Input::get('gender'));
	    if(Input::has('size'))          $variants = $variants->whereIn('variants.size_id', Input::get('size'));
	    if(Input::has('color'))         $variants = $variants->whereIn('variants.color_id', Input::get('color'));
	    if(Input::has('condition'))     $variants = $variants->whereIn('variants.condition_id', Input::get('condition'));
	    if(Input::has('brand'))         $variants = $variants->whereIn('products.brand_id', Input::get('brand'));

	    // Active & Approved
	    $variants = $variants->where('variants.is_visible', '1');
	    $variants = $variants->where('variants.is_approved', '1');

	    // In Stock
	    $variants = $variants->where('variants.quantity', '>', 0);
	    
	    // Execute query
	    $variants = $variants->get();

	    // Prepare for Paginator: Store objects in an array
	    $filtered_variants = [];
	    foreach($variants as $variant) {
	        $filtered_variants[] = Variant::find($variant->id);
	    }

	    // Pagination
	    $paginated_variants = Paginator::make($filtered_variants, count($filtered_variants), 15);


	    return View::make('store.browse-variants')
	            ->with('variants', $paginated_variants);

	}





	/**
	 * Display individual Variant
	 * @param  int $id  	Variant ID
	 * @return void
	 */
	public function getView($id) {

	    $variant = Variant::find($id);
	    
	    return View::make('store.variant')
	            ->with('variant', $variant)
	            ->with('product', $variant->product);

	}




	/**
	 * Show Shopping Cart
	 * @return void
	 */
	public function getCart() {
		// Remove items in cart that have Sold out or been Deleted
		$showAlertMsg = 0;

		if(Cart::count() != 0) {
			foreach(Cart::content() as $item) {
				if(!Variant::find($item->id) || Variant::find($item->id)->quantity < 1) {
					$showAlertMsg = 1;
					Cart::remove($item->rowid);
				}
			}
		}

		return View::make('store.cart')
					->with('showAlertMsg', $showAlertMsg);
	}





	/**
	 * Select Guest vs Registered Checkout
	 * @return void
	 */
	public function getCheckout() {
		return View::make('store.checkout');
	}





	/**
	 * Fill in User & Payment information
	 * @return void
	 */
	public function getCheckoutShipping() {

		// Remove items in cart that have Sold out or been Deleted
		$showAlertMsg = 0;

		if( Cart::count() != 0) {
			foreach(Cart::content() as $item) {
				if(!Variant::find($item->id) || Variant::find($item->id)->quantity < 1) {
					$showAlertMsg = 1;
					Cart::remove($item->rowid);
				}
			}

			// Return viewe depending on item left in cart
			if( Cart::count() > 0) {
				return View::make('store.checkout-shipping')
						->with('showAlertMsg', $showAlertMsg);
			} else {
				return View::make('store.checkout-soldout');
			}

		} else {
			// If empty cart, redirect away
			return Redirect::to('/browse');
		}
	}





	/**
	 * Process Order and Payment
	 * @return void
	 */
	public function postCheckoutProcess() {

		// Remove items in cart that have Sold out or been Deleted
		$showAlertMsg = 0;
		
		if( Cart::count() != 0) {
			foreach(Cart::content() as $item) {
				if(!Variant::find($item->id) || Variant::find($item->id)->quantity < 1) {
					$showAlertMsg = 1;
					Cart::remove($item->rowid);
				}
			}

			// Halt order if item no longer available for sale
			if($showAlertMsg) {
				return View::make('store.checkout-soldout');
			}
		}



		// Proceed to process Order

		$post = Input::all();

		// Calculate Costs
		$subtotal = 0;
		foreach(Input::get('variant_ids') as $variant_id) {
			$subtotal += Variant::find($variant_id)->price;
		}
		$tax = 0;	// TODO: Calculate tax
		$shipping = 3;	//TODO: Shipping cost depends on no. of items
		$total = $subtotal + $shipping + $tax;


		// Create Customer (if not aleady exists)
		$customer = Customer::where('email', Input::get('email'))->first();

		// If does not exist, create new Customer
		if(!$customer) {
			$data = array(
				'first_name'     => Input::get('first_name'),
				'last_name' 	 => Input::get('last_name'),
				'email' 		 => Input::get('email'),
		        'phone'          => Input::get('phone'),
				'address_1'		 => Input::get('address_1'),
				'address_2'      => Input::get('address_2'),
				'city'			 => Input::get('city'),
				'state'			 => Input::get('state'),
				'postal'		 => Input::get('postal'),
		        'country'        => Input::get('country')
			);
			$customer = Customer::create($data);
		}


		// Record Order (Guest Checkouts)
		$data = array(
	        'subtotal'       => $subtotal,
	        'tax'			 => $tax,
	        'shipping'		 => $shipping,
	        'customer_id'	 => $customer->id,
			'first_name'     => Input::get('first_name'),
			'last_name' 	 => Input::get('last_name'),
			'email' 		 => Input::get('email'),
	        'phone'          => Input::get('phone'),
			'address_1'		 => Input::get('address_1'),
			'address_2'      => Input::get('address_2'),
			'city'			 => Input::get('city'),
			'state'			 => Input::get('state'),
			'postal'		 => Input::get('postal'),
	        'country'        => Input::get('country')
		);
	    $order = Order::create($data);

	    // Sync Order Variant lines
	    foreach(Input::get('variant_ids') as $variant_id) {
	        $data = array(
	            'order_id'      => $order->id,
	            'variant_id'    => $variant_id,
	            'shop_id'       => Variant::find($variant_id)->shop->id,
	            'price'			=> Variant::find($variant_id)->price,
	            'quantity'		=> 1,			// TODO: Make it dynamic
	            'fulfillment' 	=> 'pending'
	        );
	        $orderVariant = OrderVariant::create($data);
	    }

	    // Decrement of Variant quantity
	    foreach(Input::get('variant_ids') as $variant_id) {
	    	$variant = Variant::find($variant_id);
	    	$variant->quantity -= 1;	// TODO: Make it dynamic
	    	$variant->update();
	    }


	    // Record Transaction
	    $data = array(
	        'order_id'      => $order->id,
	        'amount'        => $total,
	        'stripe_token'  => Input::get('stripeToken'),
	        'status'        => ''
	    );
	    $transaction = Transaction::create($data);


		// Set Stripe API Key
		Stripe::setApiKey("sk_test_1b8aZWoxGpdDNbx5Wr1eGneQ");

		// Create the charge on Stripe's servers
		try {
			$charge = Stripe_Charge::create(array(
				"amount" => $total * 100, 	// convert $ to cents
				"currency" => "usd",
				"card" => Input::get('stripeToken'),
				"description" => "payinguser@example.com")
			);
		} catch(Stripe_CardError $e) {
			// The card has been declined
				$transaction->status = 'declined';
				$transaction->save();
		}

	    // Mark transaction as a Success
	    $transaction->status = 'Paid';
	    $transaction->save();


	    // Empty Cart
	    Cart::destroy();


		return View::make('store.checkout-success');

	}





	/**
	 * Display Shop's Store page
	 * @param  int $shopId  	Id of Shop
	 * @return none
	 */
	public function getShop($shopId) {

	    $variants = Variant::where('shop_id', $shopId)->get();

	    return View::make('store.store-variants')
	            ->with('variants', $variants);
	}




}
