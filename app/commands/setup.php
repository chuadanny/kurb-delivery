<?php

/**
* Basic setup routine after doing a `git pull`
* - Downloads composer.phar
* - composer install
* - Fixes permissions messed up by git pull
*/


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class setup extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'setup';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fixes the file permissions.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		// Installs composer
        if(!file_exists('composer.phar')) {
            exec('curl -s https://getcomposer.org/installer | php');
            $this->info("Composer installed.\n");
        }


        // Fixes file permissions
        if(file_exists('/var/www/kurb')) {
	        exec('find /var/www/kurb -type d -exec chmod 2775 {} +');
	        exec('find /var/www/kurb -type f -exec chmod 0664 {} +');
	        exec('chmod 775 /var/www/kurb/app/storage');
	    }

	    if(file_exists('/var/www/dev')) {
	        exec('find /var/www/dev -type d -exec chmod 2775 {} +');
	        exec('find /var/www/dev -type f -exec chmod 0664 {} +');
	        exec('chmod 775 /var/www/dev/app/storage');
	    }

        // Fixes permissions for Resque workers
        // exec('chmod 775 /var/www/kurb/resque_*');
        //exec('chmod 775 /var/www/dev/resque_*.sh');

        $this->info("File permissions fixed\n");

        // // Make phantomjs executable
        // exec('chmod 775 /var/www/sephantom/application/phantomjs/phantomjs');
        // echo("PhantomJS made executable.\n");

        echo('Setup done.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}