<?php

use Illuminate\Database\Migrations\Migration;

class CreateCustomerNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_notes', function($table) {
			$table->increments('id');
			$table->integer('customer_id');
			$table->integer('shop_id');
			$table->text('notes');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_notes');
	}

}