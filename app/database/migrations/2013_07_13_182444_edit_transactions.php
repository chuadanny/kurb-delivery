<?php

use Illuminate\Database\Migrations\Migration;

class EditTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function($table) {
			$table->decimal('tax');
			$table->decimal('shipping');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function($table) {
			$table->dropColumn('tax', 'shipping');
		});
	}

}