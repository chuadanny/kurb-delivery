<?php

use Illuminate\Database\Migrations\Migration;

class EditOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function($table) {
			$table->renameColumn('total', 'subtotal');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table->renameColumn('subtotal', 'total');
	}

}