<?php

class Image {

	/**
	 * Resizes image to a fixed width
	 * @param  string $src   Path to image that will be resized
	 * @param  string $dest  Path to save resized image to
	 * @param  int $width 	 Width to resize to
	 * @return [type]        
	 */
	public static function resizeImage($src, $dest, $new_width) {

		// Dont do anything if image is 0 byte
		if(filesize($src)==0)
			return;

		// Get image dimensions
		list($img_width, $img_height) = getimagesize($src);

		
		// Resize & Auto-rotate (without checking if original is smaller than final dimensions)
		//  - EXIF Orientation tag caused photos to auto-rotate under Chrome and IE
		//  - Auto rotates based on EXIF meta-data
		$cmd = "convert ".$src." -auto-orient -resize ".$new_width." ".$dest;
		exec($cmd);

	}


	/**
	 * Resizes image to XLarge (768x1024) and Saves
	 * @param  str $src  	Full path of source image file
	 * @param  str $dest 	Full path of destination image file
	 * @return void   
	 */
	public static function resizeXlarge($src, $dest) {
		self::resizeImage($src, $dest, 768);
	}


	/**
	 * Resizes image to Large (360x480) and Saves
	 * @param  str $src  	Full path of source image file
	 * @param  str $dest 	Full path of destination image file
	 * @return void   
	 */
	public static function resizeLarge($src, $dest) {
		self::resizeImage($src, $dest, 360);
	}


	/**
	 * Resizes image to Medium (220x293) and Saves
	 * @param  str $src  	Full path of source image file
	 * @param  str $dest 	Full path of destination image file
	 * @return void   
	 */
	public static function resizeMedium($src, $dest) {
		self::resizeImage($src, $dest, 220);
	}


	/**
	 * Resizes image to Small (128x171) and Saves
	 * @param  str $src  	Full path of source image file
	 * @param  str $dest 	Full path of destination image file
	 * @return void   
	 */
	public static function resizeSmall($src, $dest) {
		self::resizeImage($src, $dest, 128);
	}


	/**
	 * Copies original image
	 *  - Also resaves image with the correct orientation by reading exif::orientation
	 * @param  str $src  	Full path of source image file
	 * @param  str $dest 	Full path of destination image file
	 * @return void       
	 */
	public static function copyOriginal($src, $dest) {

		copy($src, $dest);

	}
	

	/**
	 * Creates a square thumbnail
	 * @param  string $src        Path to image that will be resized
	 * @param  string $dest       Path to save thumbnail to
	 * @param  int $thumb_size    Width of thumbnail
	 * @return boolean
	 */
	public static function createSquareThumb($src, $dest, $thumb_size) {

		// Dont do anything if image is 0 byte
		if(filesize($src)==0)
			return;

		// Create thumbnail & auto-rotate
		//  - Auto rotates based on EXIF meta-data
		$cmd = "convert ".$src." -auto-orient -thumbnail ".$thumb_size."x".$thumb_size."^ -gravity center -extent ".$thumb_size."x".$thumb_size." ".$dest;
		exec($cmd);
		return true;

	}


	public static function importDate() {
		return '';
	}


	/**
	 * Constructs the full path to a photo
	 * @param  obj $Photo 	The Photo object
	 * @return str         	URL
	 */
	public static function getFullPath($photo) {
		return 'http://' . $photo->img_server . $photo->img_bucket . $photo->img_subpath . $photo->img_filename_original;
	}


	/**
	 * Constructs the full path to the Small version of the photo
	 * @param  obj $Photo 	The Photo object
	 * @return str         	URL
	 */
	public static function getSmallPhoto($photo) {
		return 'http://' . $photo->img_server . $photo->img_bucket . $photo->img_subpath . $photo->img_filename_small;
	}


	/**
	 * Constructs the full path to the Medium version of the photo
	 * @param  obj $Photo 	The Photo object
	 * @return str         	URL
	 */
	public static function getMediumPhoto($photo) {
		return 'http://' . $photo->img_server . $photo->img_bucket . $photo->img_subpath . $photo->img_filename_medium;
	}


	/**
	 * Constructs the full path to the Large version of the photo
	 * @param  obj $Photo 	The Photo object
	 * @return str         	URL
	 */
	public static function getLargePhoto($photo) {
		return 'http://' . $photo->img_server . $photo->img_bucket . $photo->img_subpath . $photo->img_filename_large;
	}


}
