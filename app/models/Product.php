<?php

class Product extends Eloquent {

	protected $guarded = array('');

	public function brand() {
		return $this->belongsTo('Brand');
	}

	public function style() {
		return $this->belongsTo('Style');
	}

	public function gender() {
		return $this->belongsTo('Gender');
	}

	public function size() {
		return $this->belongsTo('Size');
	}

	public function color() {
		return $this->belongsTo('Color');
	}

	public function material() {
		return $this->belongsTo('Material');
	}

	public function category() {
		return $this->belongsTo('Category');
	}

	public function orders() {
		return $this->belongsToMany('Order')
					->withTimestamps();
	}

	public function shops() {
		return $this->belongsToMany('Shop')
					->withTimestamps();
	}

	public function variants() {
		return $this->hasMany('Variant');
	}

	public function photos() {
		return $this->hasMany('Photo');
	}





	// Query Scopes

	public function scopeCategory($query, $category_id) {
		return $query->where('category_id', $category_id);
	}

	public function scopeBrand($query, $brand_id) {
		return $this->where('brand_id', $brand_id);
	}

	public function scopeName($query, $queryString) {
		return $this->where('name', 'like', '%'.$queryString.'%');
	}

}