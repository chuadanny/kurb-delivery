<?php

class Fulfillment extends Eloquent {

	protected $guarded = array('');

	public function order_product() {
		return $this->belongsTo('Order_Product', 'order_product_id');
	}

	public function order_variant() {
		return $this->belongsTo('Order_Variant', 'order_variant_id');
	}
}