<?php

class Transaction extends Eloquent {

	protected $guarded = array('');

	public function order() {
		return $this->belongsTo('Order');
	}
}