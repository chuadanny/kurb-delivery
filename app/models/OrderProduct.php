<?php

class OrderProduct extends Eloquent {

	protected $guarded = array('');
	protected $table = 'order_product';	// default is order_products

	public function fulfilment() {
		return $this->hasOne('Fulfilment');
	}

	public function order() {
		return $this->belongsTo('Order');
	}
}