<?php

class Order extends Eloquent {

	protected $guarded = array('');

	public function transaction() {
		return $this->hasOne('Transaction');
	}

	public function orderVariants() {
		return $this->hasMany('OrderVariant');
	}

	public function customer() {
		return $this->belongsTo('Customer');
	}

	// public function fulfillment() {
	// 	return $this->hasOne('Fulfillment');
	// }


	public function fulfillment() {
		$status = 'Pending';
		$itemsFulfilled = 0;
		
		foreach($this->orderVariants as $orderVariant) {
			if($orderVariant->fulfillment == 'fulfilled') {
				$status = 'Partial';
				$itemsFulfilled++;
			}
		}

		if($itemsFulfilled == count($this->orderVariants)) {
			$status = 'Fulfilled';
		}

		return $status;
	}


	public function vendorFulfillment() {
		$status = 'Pending';
		$itemsFulfilled = 0;
		$shop = Auth::user()->shop;
		$shopOrderVariants = $this->orderVariants()->where('shop_id', $shop->id)->get();

		foreach($shopOrderVariants as $orderVariant) {
			if($orderVariant->fulfillment == 'fulfilled') {
				$status = 'Partial';
				$itemsFulfilled++;
			}
		}

		if($itemsFulfilled == count($shopOrderVariants)) {
			$status = 'Fulfilled';
		}

		return $status;
	}


	public function products() {
		return $this->belongsToMany('Product')
					->withTimestamps();
	}

	public function orderProduct() {
		return $this->hasMany('OrderProduct');
	}

	public function variants() {
		return $this->belongsToMany('Variant')
					->withTimestamps();
	}



	public function total() {
		return number_format($this->subtotal + $this->shipping + $this->tax, 2);
	}

	// Formats phone number
	public function phone() {
		$number = $this->phone;
		return preg_replace('~.*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4}).*~', '($1) $2-$3', $number);
	}
}