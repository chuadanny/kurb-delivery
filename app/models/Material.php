<?php

class Material extends Eloquent {

	public function variant() {
		return $this->belongsTo('Variant');
	}
}