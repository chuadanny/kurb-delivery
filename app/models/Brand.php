<?php

class Brand extends Eloquent {

	public function products() {
		return $this->hasMany('Product');
	}
}