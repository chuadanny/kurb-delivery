<?php

class Shop extends Eloquent {

	public function user() {
		return $this->belongsTo('User');
	}

	public function products() {
		return $this->hasMany('Product');
	}

	public function variants() {
		return $this->hasMany('Variant');
	}

	public function orderVariants() {
		return $this->hasMany('OrderVariant');
	}




	public function formattedPhone() {
		// Format phone number
		$numbers = explode("\n", '(111) 222-3333
		((111) 222-3333
		1112223333
		111 222-3333
		111-222-3333
		(111)2223333
		+11234567890
		    1-8002353551
		    123-456-7890   -Hello!
		+1 - 1234567890 
		');

		return preg_replace('~.*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4}).*~', '($1) $2-$3', $this->phone). "\n";
	}

	public function salesThisWeek() {
		$sales = $this->orderVariants()
					->where('created_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL 1 WEEK)'))
					->sum('price');
		return number_format($sales, 2);
	}

	public function ordersThisWeek() {
		$orders = $this->orderVariants()
					->where('created_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL 1 WEEK)'))
					->select(DB::raw('count(DISTINCT(order_id)) as count'))
					->first();
		return $orders->count;
	}


	public function salesThisMonth() {
		$sales = $this->orderVariants()
					->where('created_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL 1 MONTH)'))
					->sum('price');
		return number_format($sales, 2);
	}

	public function ordersThisMonth() {
		$orders = $this->orderVariants()
					->where('created_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL 1 MONTH)'))
					->select(DB::raw('count(DISTINCT(order_id)) as count'))
					->first();
		return $orders->count;
	}


	public function openOrders() {
		$orders = $this->orderVariants()
						->where('fulfillment', 'pending')
						->groupBy('order_id')
						->count();

		return (int)$orders;
	}
}