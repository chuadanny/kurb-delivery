<?php

class Style extends Eloquent {

	public function products() {
		return $this->hasMany('Product');
	}
}