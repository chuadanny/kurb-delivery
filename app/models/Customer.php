<?php

class Customer extends Eloquent {

	protected $guarded = array('');

	public function orders() {
		return $this->hasMany('Order');
	}

	public function customerNotes() {
		return $this->hasMany('CustomerNotes');
	}





	

	// Formats phone number
	public function phone() {
		$number = $this->phone;
		return preg_replace('~.*(\d{3})[^\d]*(\d{3})[^\d]*(\d{4}).*~', '($1) $2-$3', $number);
	}


	// The number of orders associated with this customer
	public function ordersCount() {
		return Order::where('customer_id', $this->id)->count();
	}

	// The number of orders associated with this customer for a specific Shop
	public function ordersCountInShop($shop_id) {
		$ordersCount = OrderVariant::join('orders', 'orders.id', '=', 'order_variant.order_id')
						->where('order_variant.shop_id', $shop_id)
						->where('orders.customer_id', $this->id)
						->groupBy('orders.id')
						->get();

		return count($ordersCount);
	}


	// TOOD
	public function productOrderedCount() {

	}

	// The number of products ordered associated with this customer for a specific Shop
	public function productOrderedCountInShop($shop_id) {
		$productOrderedCount = OrderVariant::join('orders', 'orders.id', '=', 'order_variant.order_id')
						->where('order_variant.shop_id', $shop_id)
						->where('orders.customer_id', $this->id)
						->count();

		return $productOrderedCount;
	}


	// Total amount of money (excl shipping & tax) that the customer has spent
	public function totalSpent() {
		$orders = Order::where('customer_id', $this->id)->get();

		$total = 0;
		foreach($orders as $order) {
			$total += $order->subtotal + $order->shipping;
		}

		return $total;
	}
	
	// Total amount of money (excl shipping & tax) that the customer has spent for a specific Shop
	public function totalSpentInShop($shop_id) {
		$total = OrderVariant::join('orders', 'orders.id', '=', 'order_variant.order_id')
					->where('order_variant.shop_id', $shop_id)
					->where('orders.customer_id', $this->id)
					->sum('order_variant.price');

		return number_format($total, 2);
	}
}