<?php

class Variant extends Eloquent {

	protected $guarded = array('');

	public function product() {
		return $this->belongsTo('Product');
	}

	public function shop() {
		return $this->belongsTo('Shop');
	}

	public function size() {
		return $this->belongsTo('Size');
	}

	public function color() {
		return $this->belongsTo('Color');
	}

	public function condition() {
		return $this->belongsTo('Condition');
	}

	public function photos() {
		return $this->hasMany('Photo');
	}

	public function approval() {
		return $this->hasOne('Approval');
	}
	
	public function orders() {
		return $this->belongsToMany('Order')
					->withTimestamps();
	}

	public function orderVariant() {
		return $this->hasMany('OrderVariant');
	}
	
}