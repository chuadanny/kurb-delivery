<?php

class Condition extends Eloquent {

	public function variants() {
		return $this->hasMany('Variant');
	}
}