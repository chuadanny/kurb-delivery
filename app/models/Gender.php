<?php

class Gender extends Eloquent {

	public function products() {
		return $this->hasMany('Product');
	}
}