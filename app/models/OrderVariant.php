<?php

class OrderVariant extends Eloquent {

	protected $guarded = array('');
	protected $table = 'order_variant';	// default is order_products

	public function fulfilment() {
		return $this->hasOne('Fulfilment');
	}

	public function order() {
		return $this->belongsTo('Order');
	}

	public function variant() {
		return $this->belongsTo('Variant');
	}

	public function shop() {
		return $this->belongsTo('Shop');
	}
}