<?php

class Photo extends Eloquent {

	protected $guarded = array('');

	public function product() {
		return $this->belongsTo('Product');
	}

	public function variant() {
		return $this->belongsTo('Variant');
	}










	public function removeAllFiles() {

		$filesToDelete = [
			$this->img_root . $this->img_bucket . $this->img_subpath . $this->img_filename_original,
			$this->img_root . $this->img_bucket . $this->img_subpath . $this->img_filename_xlarge,
			$this->img_root . $this->img_bucket . $this->img_subpath . $this->img_filename_large,
			$this->img_root . $this->img_bucket . $this->img_subpath . $this->img_filename_medium,
			$this->img_root . $this->img_bucket . $this->img_subpath . $this->img_filename_small
		];

		foreach($filesToDelete as $file) {
			if(file_exists($file))
				unlink($file);
		}

	}
}