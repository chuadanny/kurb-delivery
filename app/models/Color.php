<?php

class Color extends Eloquent {

	public function products() {
		return $this->hasMany('Product');
	}

	public function variants() {
		return $this->hasMany('Variant');
	}
}