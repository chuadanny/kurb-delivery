<?php

class Approval extends Eloquent {

	protected $guarded = array('');

	public function variant() {
		return $this->belongsTo('Variant');
	}
}