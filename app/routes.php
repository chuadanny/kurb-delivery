<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/












/*
|--------------------------------------------------------------------------
| Random stuff
|--------------------------------------------------------------------------
*/
Route::get('phpinfo', function() {
    echo phpinfo();
});


Route::get('test', function() {
    echo base_path();
});

Route::get('testcustomer', function() {
    echo '<pre>';

    $order = Order::find('1000094');
    // print_r($order);
    // print_r($order->customer);
    echo $order->customer->id;
});

Route::get('clearcart', function() {
    Cart::destroy();
});










/*
|--------------------------------------------------------------------------
| Controller Routes
|--------------------------------------------------------------------------
*/

Route::controller('admin', 'AdminController');
Route::controller('admin-api', 'AdminApiController');

Route::group(['prefix' => 'api/v1', 'auth' => 'adminAuth'], function() {
    Route::resource('customers', 'api_CustomersController');
    Route::resource('orders', 'api_OrdersController');
    Route::resource('variants', 'api_VariantsController');
});


Route::controller('superadmin', 'SuperadminController');
Route::controller('superadmin-api', 'SuperadminApiController');

Route::controller('shops', 'ShopController');


Route::controller('api', 'ApiController');
Route::controller('/', 'StoreController');











/*
|--------------------------------------------------------------------------
| Form Macros
|--------------------------------------------------------------------------
*/

Form::macro('states', function($name = "state", $selected = null) {
	
	$states = array(
		''=>"State",
		'AL'=>"Alabama",  
		'AK'=>"Alaska",  
		'AZ'=>"Arizona",  
		'AR'=>"Arkansas",  
		'CA'=>"California",  
		'CO'=>"Colorado",  
		'CT'=>"Connecticut",  
		'DE'=>"Delaware",  
		'DC'=>"District Of Columbia",  
		'FL'=>"Florida",  
		'GA'=>"Georgia",  
		'HI'=>"Hawaii",  
		'ID'=>"Idaho",  
		'IL'=>"Illinois",  
		'IN'=>"Indiana",  
		'IA'=>"Iowa",  
		'KS'=>"Kansas",  
		'KY'=>"Kentucky",  
		'LA'=>"Louisiana",  
		'ME'=>"Maine",  
		'MD'=>"Maryland",  
		'MA'=>"Massachusetts",  
		'MI'=>"Michigan",  
		'MN'=>"Minnesota",  
		'MS'=>"Mississippi",  
		'MO'=>"Missouri",  
		'MT'=>"Montana",
		'NE'=>"Nebraska",
		'NV'=>"Nevada",
		'NH'=>"New Hampshire",
		'NJ'=>"New Jersey",
		'NM'=>"New Mexico",
		'NY'=>"New York",
		'NC'=>"North Carolina",
		'ND'=>"North Dakota",
		'OH'=>"Ohio",  
		'OK'=>"Oklahoma",  
		'OR'=>"Oregon",  
		'PA'=>"Pennsylvania",  
		'RI'=>"Rhode Island",  
		'SC'=>"South Carolina",  
		'SD'=>"South Dakota",
		'TN'=>"Tennessee",  
		'TX'=>"Texas",  
		'UT'=>"Utah",  
		'VT'=>"Vermont",  
		'VA'=>"Virginia",  
		'WA'=>"Washington",  
		'WV'=>"West Virginia",  
		'WI'=>"Wisconsin",  
		'WY'=>"Wyoming"
	);
	
	$select = '<select name="'.$name.'">';
	
	foreach ($states as $abbr => $state)
	{
		$select .= '<option value="'.$abbr.'"'.($selected == $abbr ? ' selected="selected"' : '').'>'.$state.'</option> ';
	}
	
	$select .= '</select>';
	
	return $select;

});


Form::macro('countries', function($name = "country", $selected = null) {
	
	$countries = array(
		''=>"Select a Country",
		'ac'=>'Ascension Island',
    'ad'=>'Andorra',
    'ae'=>'United Arab Emirates',
    'af'=>'Afghanistan',
    'ag'=>'Antigua And Barbuda',
    'ai'=>'Anguilla',
    'al'=>'Albania',
    'am'=>'Armenia',
    'an'=>'Netherlands Antilles',
    'ao'=>'Angola',
    'aq'=>'Antarctica',
    'ar'=>'Argentina',
    'as'=>'American Samoa',
    'at'=>'Austria',
    'au'=>'Australia',
    'aw'=>'Aruba',
    'ax'=>'Ãƒâ€¦land',
    'az'=>'Azerbaijan',
    'ba'=>'Bosnia And Herzegovina',
    'bb'=>'Barbados',
    'be'=>'Belgium',
    'bd'=>'Bangladesh',
    'bf'=>'Burkina Faso',
    'bg'=>'Bulgaria',
    'bh'=>'Bahrain',
    'bi'=>'Burundi',
    'bj'=>'Benin',
    'bm'=>'Bermuda',
    'bn'=>'Brunei Darussalam',
    'bo'=>'Bolivia',
    'br'=>'Brazil',
    'bs'=>'Bahamas',
    'bt'=>'Bhutan',
    'bv'=>'Bouvet Island',
    'bw'=>'Botswana',
    'by'=>'Belarus',
    'bz'=>'Belize',
    'ca'=>'Canada',
    'cc'=>'Cocos (Keeling) Islands',
    'cd'=>'Congo (Democratic Republic)',
    'cf'=>'Central African Republic',
    'cg'=>'Congo (Republic)',
    'ch'=>'Switzerland',
    'ci'=>'Cote DÃ¢â‚¬â„¢Ivoire',
    'ck'=>'Cook Islands',
    'cl'=>'Chile',
    'cm'=>'Cameroon',
    'cn'=>'PeopleÃ¢â‚¬â„¢s Republic of China',
    'co'=>'Colombia',
    'cr'=>'Costa Rica',
    'cu'=>'Cuba',
    'cv'=>'Cape Verde',
    'cx'=>'Christmas Island',
    'cy'=>'Cyprus',
    'cz'=>'Czech Republic',
    'de'=>'Germany',
    'dj'=>'Djibouti',
    'dk'=>'Denmark',
    'dm'=>'Dominica',
    'do'=>'Dominican Republic',
    'dz'=>'Algeria',
    'ec'=>'Ecuador',
    'ee'=>'Estonia',
    'eg'=>'Egypt',
    'er'=>'Eritrea',
    'es'=>'Spain',
    'et'=>'Ethiopia',
    'eU'=>'European Union',
    'fi'=>'Finland',
    'fj'=>'Fiji',
    'fk'=>'Falkland Islands (Malvinas)',
    'fm'=>'Micronesia, Federated States Of',
    'fo'=>'Faroe Islands',
    'fr'=>'France',
    'ga'=>'Gabon',
    'gb'=>'United Kingdom',
    'gd'=>'Grenada',
    'ge'=>'Georgia',
    'gf'=>'French Guiana',
    'gg'=>'Guernsey',
    'gh'=>'Ghana',
    'gi'=>'Gibraltar',
    'gl'=>'Greenland',
    'gm'=>'Gambia',
    'gn'=>'Guinea',
    'gp'=>'Guadeloupe',
    'gq'=>'Equatorial Guinea',
    'gr'=>'Greece',
    'gs'=>'South Georgia And The South Sandwich Islands',
    'gt'=>'Guatemala',
    'gu'=>'Guam',
    'gw'=>'Guinea-Bissau',
    'gy'=>'Guyana',
    'hk'=>'Hong Kong',
    'hm'=>'Heard And Mc Donald Islands',
    'hn'=>'Honduras',
    'hr'=>'Croatia (local name: Hrvatska)',
    'ht'=>'Haiti',
    'hu'=>'Hungary',
    'id'=>'Indonesia',
    'ie'=>'Ireland',
    'il'=>'Israel',
    'im'=>'Isle of Man',
    'in'=>'India',
    'io'=>'British Indian Ocean Territory',
    'iq'=>'Iraq',
    'ir'=>'Iran (Islamic Republic Of)',
    'is'=>'Iceland',
    'it'=>'Italy',
    'je'=>'Jersey',
    'jm'=>'Jamaica',
    'jo'=>'Jordan',
    'jp'=>'Japan',
    'ke'=>'Kenya',
    'kg'=>'Kyrgyzstan',
    'kh'=>'Cambodia',
    'ki'=>'Kiribati',
    'km'=>'Comoros',
    'kn'=>'Saint Kitts And Nevis',
    'kr'=>'Korea, Republic Of',
    'kw'=>'Kuwait',
    'ky'=>'Cayman Islands',
    'kz'=>'Kazakhstan',
    'la'=>'Lao PeopleÃ¢â‚¬â„¢s Democratic Republic',
    'lb'=>'Lebanon',
    'lc'=>'Saint Lucia',
    'li'=>'Liechtenstein',
    'lk'=>'Sri Lanka',
    'lr'=>'Liberia',
    'ls'=>'Lesotho',
    'lt'=>'Lithuania',
    'lu'=>'Luxembourg',
    'lv'=>'Latvia',
    'ly'=>'Libyan Arab Jamahiriya',
    'ma'=>'Morocco',
    'mc'=>'Monaco',
    'md'=>'Moldova, Republic Of',
    'me'=>'Montenegro',
    'mg'=>'Madagascar',
    'mh'=>'Marshall Islands',
    'mk'=>'Macedonia, The Former Yugoslav Republic Of',
    'ml'=>'Mali',
    'mm'=>'Myanmar',
    'mn'=>'Mongolia',
    'mo'=>'Macau',
    'mp'=>'Northern Mariana Islands',
    'mq'=>'Martinique',
    'mr'=>'Mauritania',
    'ms'=>'Montserrat',
    'mt'=>'Malta',
    'mu'=>'Mauritius',
    'mv'=>'Maldives',
    'mw'=>'Malawi',
    'mx'=>'Mexico',
    'my'=>'Malaysia',
    'mz'=>'Mozambique',
    'na'=>'Namibia',
    'nc'=>'New Caledonia',
    'ne'=>'Niger',
    'nf'=>'Norfolk Island',
    'ng'=>'Nigeria',
    'ni'=>'Nicaragua',
    'nl'=>'Netherlands',
    'no'=>'Norway',
    'np'=>'Nepal',
    'nr'=>'Nauru',
    'nu'=>'Niue',
    'nz'=>'New Zealand',
    'om'=>'Oman',
    'pa'=>'Panama',
    'pe'=>'Peru',
    'pf'=>'French Polynesia',
    'pg'=>'Papua New Guinea',
    'ph'=>'Philippines, Republic of the',
    'pk'=>'Pakistan',
    'pl'=>'Poland',
    'pm'=>'St. Pierre And Miquelon',
    'pn'=>'Pitcairn',
    'pr'=>'Puerto Rico',
    'ps'=>'Palestine',
    'pt'=>'Portugal',
    'pw'=>'Palau',
    'py'=>'Paraguay',
    'qa'=>'Qatar',
    're'=>'Reunion',
    'ro'=>'Romania',
    'rs'=>'Serbia',
    'ru'=>'Russian Federation',
    'rw'=>'Rwanda',
    'sa'=>'Saudi Arabia',
    'uk'=>'Scotland',
    'sb'=>'Solomon Islands',
    'sc'=>'Seychelles',
    'sd'=>'Sudan',
    'se'=>'Sweden',
    'sg'=>'Singapore',
    'sh'=>'St. Helena',
    'si'=>'Slovenia',
    'sj'=>'Svalbard And Jan Mayen Islands',
    'sk'=>'Slovakia (Slovak Republic)',
    'sl'=>'Sierra Leone',
    'sm'=>'San Marino',
    'sn'=>'Senegal',
    'so'=>'Somalia',
    'sr'=>'Suriname',
    'st'=>'Sao Tome And Principe',
    'su'=>'Soviet Union',
    'sv'=>'El Salvador',
    'sy'=>'Syrian Arab Republic',
    'sz'=>'Swaziland',
    'tc'=>'Turks And Caicos Islands',
    'td'=>'Chad',
    'tf'=>'French Southern Territories',
    'tg'=>'Togo',
    'th'=>'Thailand',
    'tj'=>'Tajikistan',
    'tk'=>'Tokelau',
    'ti'=>'East Timor (new code)',
    'tm'=>'Turkmenistan',
    'tn'=>'Tunisia',
    'to'=>'Tonga',
    'tp'=>'East Timor (old code)',
    'tr'=>'Turkey',
    'tt'=>'Trinidad And Tobago',
    'tv'=>'Tuvalu',
    'tw'=>'Taiwan',
    'tz'=>'Tanzania, United Republic Of',
    'ua'=>'Ukraine',
    'ug'=>'Uganda',
    'uk'=>'United Kingdom',
    'um'=>'United States Minor Outlying Islands',
    'us'=>'United States',
    'uy'=>'Uruguay',
    'uz'=>'Uzbekistan',
    'va'=>'Vatican City State (Holy See)',
    'vc'=>'Saint Vincent And The Grenadines',
    've'=>'Venezuela',
    'vg'=>'Virgin Islands (British)',
    'vi'=>'Virgin Islands (U.S.)',
    'vn'=>'Viet Nam',
    'vu'=>'Vanuatu',
    'wf'=>'Wallis And Futuna Islands',
    'ws'=>'Samoa',
    'ye'=>'Yemen',
    'yt'=>'Mayotte',
    'za'=>'South Africa',
    'zm'=>'Zambia',
    'zw'=>'Zimbabwe'
	);
	
	$select = '<select name="'.$name.'">';
	
	foreach ($countries as $abbr => $country)
	{
		$select .= '<option value="'.$abbr.'"'.($selected == $abbr ? ' selected="selected"' : '').'>'.$country.'</option> ';
	}
	
	$select .= '</select>';
	
	return $select;

});