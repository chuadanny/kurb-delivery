<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">
                <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="content span12">


                    <div class="row">

                        <div class="shop-main-img">
                            <img src="{{ $shop->img_filename_main }}" />
                        </div>

                        <h1>{{ $shop->name }}</h1>
                        <h3>{{ $shop->address_1 }} 
                            {{ $shop->city }} {{ $shop->state }} {{ $shop->postal }}</h3>
                        <p>{{ $shop->description }}</p>


                        <p><b>Phone:</b> {{ $shop->formattedPhone() }}</p>
                        <p><b>Things to do nearby:</b> {{ $shop->things_to_do_nearby }}</p>


                        <div class="products shop-featured-products">

                            @foreach($variants as $variant)
                                <div class="product">
                                    <a href="/view/{{ $variant['id'] }}">
                                        <img src="{{ Image::getMediumPhoto($variant->photos->first()) }}" />
                                    </a>

                                    <div class="details">
                                        <span class="brand">{{ $variant->product->brand->name }}</span>
                                        <span class="name">{{ $variant->product->name }}</span>
                                        <span class="price">${{ $variant->price }}</span>
                                        <span class="retail-price">${{ $variant->retail_price }} retail price</span>
                                    </div>
                                </div>
                            @endforeach

                        </div> <!-- .products -->

                    </div>

                </div>
                <!-- End Content -->
            </div>
            <!-- End Main -->
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/browse.js"></script>





    </body>
</html>
