<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">
                <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="content span12">


                    <div class="row">

                        <div class="shops-city-sidebar span2">
                            <h3>Cities</h3>
                            <ul class="unstyled">
                                <li><a href="#">Boston</a></li>
                            </ul>
                        </div>


                        <div class="products span10">

                            @foreach ($shops as $shop)
                                <div class="product">
                                    <a href="/shops/view/{{ $shop->id }}">
                                        <img src="{{ $shop->img_filename_small }}" width='300' />
                                    </a>

                                    <div class="details">
                                        <span class="brand">{{ $shop->city }}</span>
                                        <span class="name">{{ $shop->name }}</span>
                                    </div>
                                </div>
                            @endforeach

                        </div> <!-- End .products -->

                    </div> <!-- End .row -->

                </div>
                <!-- End Content -->
            </div>
            <!-- End Main -->
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/browse.js"></script>





    </body>
</html>
