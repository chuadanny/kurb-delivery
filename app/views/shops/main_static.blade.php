<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">
                <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="content span12">


                    <div class="row">

                        <div class="shops-city-sidebar span2">
                            <h3>Cities</h3>
                            <ul class="unstyled">
                                <li><a href="#">Boston</a></li>
                            </ul>
                        </div>



                        <div class="products">
                            <div class="products-row clearfix">

                                <div class="product">
                                    <a href="/shops/1001">
                                        <img src="https://s3.amazonaws.com/static.kurb.com.production/shops/1001_main.jpg" width='300' />
                                    </a>

                                    <div class="details">
                                        <span class="brand">Somerville</span>
                                        <span class="name">Two Little Monkeys</span>
                                    </div>
                                </div>


                                <div class="product">
                                    <a href="/shops/1002">
                                        <img src="https://s3.amazonaws.com/static.kurb.com.production/shops/1002_main.jpg" width='300' />
                                    </a>

                                    <div class="details">
                                        <span class="brand">Brookline</span>
                                        <span class="name">Fancy Pants</span>
                                    </div>
                                </div>


                                <div class="product">
                                    <a href="/shops/1003">
                                        <img src="https://s3.amazonaws.com/static.kurb.com.production/shops/1003_main.jpg" width='300' />
                                    </a>

                                    <div class="details">
                                        <span class="brand">Belmont</span>
                                        <span class="name">Growing Up</span>
                                    </div>
                                </div>

                            </div>



                             <div class="products-row clearfix">

                                <div class="product">
                                    <a href="/shops/1004">
                                        <img src="https://s3.amazonaws.com/static.kurb.com.production/shops/1004_main.jpg" width='300' />
                                    </a>

                                    <div class="details">
                                        <span class="brand">Needham</span>
                                        <span class="name">Cherry Picked</span>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div> <!-- end .products -->




                </div>
                <!-- End Content -->
            </div>
            <!-- End Main -->
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/browse.js"></script>





    </body>
</html>
