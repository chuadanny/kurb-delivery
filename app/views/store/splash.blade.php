<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')
        <link rel="stylesheet" href="/css/splash.css">


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            
            

            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                <div class="main">

                    <div class="row">
                        <div class="span12 splash-hero">

                            <div class="splash-toolbar">
                                <ul class="unstyled">
                                    <li><a href="/cart">Cart</a></li>
                                    <li><a href="/checkout">Checkout ({{ Cart::count() }})</a></li>
                                </ul>
                            </div>

                            <div style="overflow: hidden; height: 0px; width: 0px;">.</div>

                            <div class="row">
                                <div class="span4">
                                    <div class="splash-hero-sidebar">
                                        <div class="splash-logo"></div>
                                        <div class="title-01">Resale Redefined</div>
                                        <div class="title-02">Luxury brands <br/> for little jet setters of all ages</div>

                                        <div class="splash-shop-btn"></div>

                                        <ul class="unstyled splash-cat-links">
                                            <li><a href="/browse?gender[]=1">Boys</a></li>
                                            <li><a href="/browse?gender[]=2">Girls</a></li>
                                            <li><a href="/browse?gender[]=3">Neutral</a></li>
                                            <li><a href="/shops">Explore Shops</a></li>
                                        </ul>

                                        <div class="title-01">Back to school</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="span12">
                            <div class="splash-hline"></div>
                        </div>
                    </div>

                    <div class="row splash-subhero">
                        <div class="span3">
                            <img src="/img/template/splash-newarrivals.jpg" />

                            <p>New outfits go up on Mondays. <br/>
                                Sign up for alerts and get 15% off!</p>

                            <div class="splash-hline-thin"></div>
                        </div>

                        <div class="span9">
                            <div class="splash-newarrivals-board">

                                <div class="product-frame">
                                    <a href="/view/1000073"><img src="http://s3.amazonaws.com/static.kurb.com.production/products/1000073_medium.jpg" /></a>
                                    <div class="product-description">Jumper by Hanna Andersson</div>
                                    <div class="product-price">$19.99</div>
                                </div>

                                <div class="product-frame">
                                    <a href="/view/1000074"><img src="http://s3.amazonaws.com/static.kurb.com.production/products/1000074_medium.jpg" /></a>
                                    <div class="product-description">Floral Dress by Baby Dior</div>
                                    <div class="product-price">$15.99</div>
                                </div>

                                <div class="product-frame">
                                    <a href="/view/1000075"><img src="http://s3.amazonaws.com/static.kurb.com.production/products/1000075_medium.jpg" /></a>
                                    <div class="product-description">Casual Top by Mini Boden</div>
                                    <div class="product-price">$17.99</div>
                                </div>

                                <div class="product-frame">
                                    <a href="/view/1000076"><img src="http://s3.amazonaws.com/static.kurb.com.production/products/1000076_medium.jpg" /></a>
                                    <div class="product-description">Jeans by Disel</div>
                                    <div class="product-price">$15.99</div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
            <!-- End Main -->
            
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')




    </body>
</html>
