        <div class="footer-wrapper">
            <div style="overflow: hidden; height: 0px; width: 0px;">.</div>

            <div class="footer clearfix">

                <!-- Begin footer navigation -->
                <div class="footer-menu span4">
                    <h4>Info</h4>
                    <ul class="unstyled">
                        <li><a href="/info/about-us">About Us</a></li>
                        <li><a href="/info/privacy-policy">Privacy Policy</a></li>
                        <li><a href="/info/terms-and-conditions">Terms and Conditions</a></li>
                        <li><a href="/info/become-an-ambassador">Become an Ambassador</a></li>
                    </ul>
                </div>

                <div class="footer-menu span4">
                    <h4>Customer Care</h4>
                    <ul class="unstyled">
                        <li><a href="/info/contact-us">Contact Us</a></li>
                        <li><a href="/account">My Account</a></li>
                        <li><a href="/account/order-status">Order Status</a></li>
                    </ul>
                </div>
                <!-- End footer navigation -->


                <!-- Begin newsletter/social -->
                <div class="footer-menu span4">
                    <h4>Social</h4>
                    <ul class="unstyled">
                        <li><a href="http://www.twitter.com/kurbdotcom" class="social-icon twitter">Twitter</a></li>
                        <li><a href="http://www.twitter.com/kurbdotcom" class="social-icon facebook">Facebook</a></li>
                        <li><a href="http://www.instagram.com/kurb-kids" class="social-icon instagram">Instagram</a></li>
                    </ul>  
                </div>
                <!-- End newsletter/social -->


                <!-- Copyright -->
                <div class="copyright span12">
                    Copyright © 2013 Kurb Inc. All Rights Reserved.
                </div>
                <!-- End Copyright -->

            </div>
        </div>