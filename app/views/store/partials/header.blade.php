        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
        <!-- Begin toolbar -->
        <div class="toolbar-wrapper">
            <div class="toolbar clearfix">
                <div class="row">
                    <div class="span12 clearfix">
                        <ul class="unstyled">
                            <li>
                                <i class="icon-shopping-cart icon-white"></i><a href="/cart" class="cart" title="Shopping Cart">Cart ({{ Cart::count() }})</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End toolbar -->

        <!-- Begin Header -->
        <div class="header-wrapper">
            <div class="header">
                <div class="row" id="header">
                    <div class="span12">
                        <div class="row">

                            <div class="span3">
                                <div class="logo">
                                    <a href="/"><img src="/img/template/header-logo.png" alt="Kurb" /></a>
                                </div> 
                            </div> 

                            <section id="nav" class="span6">
                                    <ul class="unstyled">
                                        <li><a href="/browse?gender[]=1">Boys</a></li>
                                        <li><a href="/browse?gender[]=2">Girls</a></li>
                                        <li><a href="/">Unisex</a></li>
                                        <li><a href="/">Accessories</a></li>
                                    </ul>
                            </section> 

                            <div class="search-bar span3">
                                <form class="search" action="/search">
                                    <input type="text" name="q" class="search_box" placeholder="Search" value="" />
                                </form>
                            </div>

                        </div>

                    </div> 
                </div> 
            </div>
        </div>
        <!-- End Header -->