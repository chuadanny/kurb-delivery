<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">
                <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="content span12">

                    <div class="filters">
                        <form method="GET" action="/browse" accept-charset="UTF-8">
                            <select id="filters-gender" name="gender[]" class="selectpicker" title='Gender' multiple data-selected-text-format="count>2">
                                @foreach(Gender::all() as $gender)
                                    <option value="{{ $gender->id }}" @if(in_array($gender->id, Input::get('gender',[]))) {{ 'selected' }} @endif>{{ $gender->name }}</option>
                                @endforeach
                            </select>
                            <select id="filters-size" name="size[]" class="selectpicker" title='Size' multiple data-selected-text-format="count>2">
                                @foreach(Size::all() as $size)
                                    <option value="{{ $size->id }}" @if(in_array($size->id, Input::get('size',[]))) {{ 'selected' }} @endif>{{ $size->name }}</option>
                                @endforeach
                            </select>
                            <select id="filters-color" name="color[]" class="selectpicker" title='Color' multiple data-selected-text-format="count>2">
                                @foreach(Color::all() as $color)
                                    <option value="{{ $color->id }}" @if(in_array($color->id, Input::get('color',[]))) {{ 'selected' }} @endif> <div></div>{{ $color->name }}</option>
                                @endforeach
                            </select>

                            <input type="submit" id="filters-submit-btn" class="btn"></input>
                        </form>
                    </div>

                    @foreach ($variants as $index => $variant)

                        @if($index == 0)
                            <div class="products-row clearfix">
                        @endif
                        @if($index % 4 == 0 && $index != 0)
                            </div>
                            <div class="products-row clearfix">
                        @endif

                        <div class="product">
                            <a href="/view/{{ $variant['id'] }}">
                                <img src="{{ Image::getMediumPhoto($variant->photos->first()) }}" />
                            </a>

                            <div class="details">
                                <span class="brand">{{ $variant->product->brand->name }}</span>
                                <span class="name">{{ $variant->product->name }}</span>
                                <span class="price">${{ $variant->price }}</span>
                                <span class="retail-price">${{ $variant->retail_price }} retail price</span>
                            </div>
                        </div>

                    @endforeach
                    </div> <!-- Closes final .products-row -->

                    {{ $variants->links(); }}

                </div>
                <!-- End Content -->
            </div>
            <!-- End Main -->
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/browse.js"></script>





    </body>
</html>
