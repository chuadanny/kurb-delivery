<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">
                <!-- Begin Sidebar -->
                <div class="sidebar">

                    <div class="title"><i class="carat-icon"></i><i class="icon-carat"></i>Clothes</div>
                    <ul class="unstyled">
                        <li><a href="/">Hipster</a></li>
                        <li><a href="/">Princessy</a></li>
                        <li><a href="/">Vintage</a></li>
                        <li><a href="/">Luxe</a></li>
                        <li><a href="/">Organic</a></li>
                        <li><a href="/">Traditional</a></li>
                        <li><a href="/">Preppy</a></li>
                        <li><a href="/">Popstar</a></li>
                        <li><a href="/">Funky European</a></li>
                    </ul>

                </div>
                <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="content span9">

                    <div class="filters">
                        <form method="GET" action="/browse" accept-charset="UTF-8">
                            <select id="filters-gender" name="gender[]" class="selectpicker" title='Gender' multiple data-selected-text-format="count>2">
                                @foreach(Gender::all() as $gender)
                                    <option value="{{ $gender->id }}" @if(in_array($gender->id, Input::get('gender',[]))) {{ 'selected' }} @endif>{{ $gender->name }}</option>
                                @endforeach
                            </select>
                            <select id="filters-size" name="size[]" class="selectpicker" title='Size' multiple data-selected-text-format="count>2">
                                @foreach(Size::all() as $size)
                                    <option value="{{ $size->id }}" @if(in_array($size->id, Input::get('size',[]))) {{ 'selected' }} @endif>{{ $size->name }}</option>
                                @endforeach
                            </select>
                            <select id="filters-color" name="color[]" class="selectpicker" title='Color' multiple data-selected-text-format="count>2">
                                @foreach(Color::all() as $color)
                                    <option value="{{ $color->id }}" @if(in_array($color->id, Input::get('color',[]))) {{ 'selected' }} @endif> <div></div>{{ $color->name }}</option>
                                @endforeach
                            </select>

                            <input type="submit" class="btn"></input>
                        </form>
                    </div>

                    @foreach ($products as $index => $product)

                        @if($index == 0)
                            <div class="products-row clearfix">
                        @endif
                        @if($index % 3 == 0 && $index != 0)
                            </div>
                            <div class="products-row clearfix">
                        @endif

                        <div class="product">
                            <a href="/shop/{{ $product['id'] }}">
                                <img src="{{ Image::getFullPath($product->photos->first()) }}" />
                            </a>

                            <div class="details">
                                <span class="brand">{{ $product->brand['name'] }}</span>
                                <span class="name">{{ $product['name'] }}</span>
                                ${{ $product['price'] }}
                                <span class="retail-price">${{ $product['retail_price'] }} retail price</span>

                            </div>
                        </div>

                    @endforeach
                    </div> <!-- Closes final .products-row -->

                    {{ $products->links(); }}

                </div>
                <!-- End Content -->
            </div>
            <!-- End Main -->
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/browse.js"></script>





    </body>
</html>
