<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')
            

            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                <h1>Your Cart</h1>

                @if($showAlertMsg)
                    <div class="alert" id="alert-msg">
                        <a class="close" data-dismiss="alert" href="#">&times;</a>
                        One or more items have been removed from your shopping cart due to insufficient quantity in stock.
                    </div>
                @endif

                <div class="row">
                    <table class="cart-contents table table-hover span12">
                        <thead>
                            <tr>
                                <th class="span2">&nbsp;</th>
                                <th class="span4">Item</th>
                                <th class="span1">Qty</th>
                                <th class="span1">Price</th>
                                <th class="span1">&nbsp;</th>
                            </tr>
                        </thead>

                        @if(Cart::count() != 0)
                            @foreach(Cart::content() as $item)
                                <tr>
                                    <td><img src="{{ Image::getFullPath(Variant::find($item->id)->photos->first()) }}" /></td>
                                    <td class="name">
                                        <span class="product-name">{{ $item->name }}</span> 
                                        <span class="brand-name">by {{ Variant::find($item->id)->product->brand->name }}</span>
                                        <span class="product-id">Style #{{$item->id}}</span>
                                    </td>
                                    <td class="options">{{ $item->qty }}</td>
                                    <td class="price">{{ $item->price }}</td>
                                    <td class="remove"><a href="#" class="remove-from-cart" data-rowid={{ $item->rowid }}>Remove</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td></td>
                                <td>Nothing in cart. <a href="/browse">Start shopping!</a></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif

                            <tr>
                                <td></td>
                                <td class="name"></td>
                                <td class="options">Subtotal: </td>
                                <td class="subtotal">${{ Cart::total() }}</td>
                                <td class="remove"></td>
                            </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="span12">
                        <a href="/browse" class="btn">Continue Shopping</a>
                        @if(Cart::count() != 0)
                            <a href="/checkout" class="btn">Checkout</a>
                        @endif
                    </div>
                </div>





            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/cart.js"></script>




    </body>
</html>
