<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')
            

            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                <h1>Check Out</h1>

                @if($showAlertMsg)
                    <div class="alert" id="alert-msg">
                        <a class="close" data-dismiss="alert" href="#">&times;</a>
                        One or more items have been removed from your shopping cart due to insufficient quantity in stock.
                    </div>
                @endif

                <div class="row main">

                    <div class="span7">
                        <form id="payment-form" class="payment-form" method="POST" action="/checkout-process">
                            <fieldset>
                                <legend>Customer Info</legend>
                                <label>Full Name</label>
                                <input type="text" name="first_name" placeholder="First Name">
                                <input type="text" name="last_name" placeholder="Last Name">
                                <label>Email</label>
                                <input type="text" name="email" placeholder="me@email.com">
                                <label>Phone</label>
                                <input type="text" name="phone" placeholder="Phone">
                            </fieldset>

                            <fieldset>
                                <legend>Shipping</legend>
                                <label>Address</label>
                                <input type="text" name="address_1" class="input-xxlarge" placeholder="Line 1"><br/>
                                <input type="text" name="address_2" class="input-xxlarge" placeholder="Line 2">
                                <label></label>
                                <input type="text" name="city" class="input-medium" placeholder="City">
                                {{ Form::states() }}
                                <input type="text" name="postal" class="input-small" placeholder="Zip">
                                <input type="hidden" name="country" value="us"></input>
                            </fieldset>

                            <fieldset>
                                <legend>Billing</legend>
                                <label class="payment-errors"></label>

                                <label>Card Number</label>
                                <input type="text" class="input-large" placeholder="Card Number" size="20" data-stripe="number"/>
                                <input type="text" class="input-small" placeholder="CVC" size="4" data-stripe="cvc"/>

                                <label>Expiration</label>
                                <input type="text" class="input-mini" placeholder="MM" size="2" data-stripe="exp-month"/>/
                                <input type="text" class="input-mini" placeholder="YYYY" size="4" data-stripe="exp-year"/>
                            </fieldset>

                            @foreach(Cart::content() as $item)
                                <input type="hidden" name="variant_ids[]" value="{{ $item->id }}"></input>
                            @endforeach

                            <button type="submit" class="btn">Place Order</button>
                        </form>
                    </div>

                    <div class="span4 offset1">
                        <h3>Order Summary</h3>
                        <table class="cart-contents table table-hover span12">
                            <thead>
                                <tr>
                                    <th class="span2">&nbsp;</th>
                                    <th class="span4">Item</th>
                                    <th class="span1">Qty</th>
                                    <th class="span1">Price</th>
                                </tr>
                            </thead>

                            @foreach(Cart::content() as $item)
                                <tr>
                                    <td class="image">
                                        <img src="{{ Image::getFullPath(Variant::find($item->id)->photos->first()) }}" />
                                    </td>
                                    <td class="name">
                                        <span class="product-name">{{ $item->name }}</span> 
                                        <span class="brand-name">by {{ Variant::find($item->id)->product->brand->name }}</span>
                                        <span class="product-id">Style #{{$item->id}}</span>
                                    </td>
                                    <td class="options">{{ $item->qty }}</td>
                                    <td class="price">{{ $item->price }}</td>
                                </tr>
                            @endforeach

                            <tr>
                                <td></td>
                                <td class="name"></td>
                                <td class="options">Subtotal: </td>
                                <td class="subtotal">${{ Cart::total() }}</td>
                            </tr>
                        </table>
                    </div>

                </div>
                
            </div>
            <!-- End Main -->
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script type="text/javascript" src="/js/checkout.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">
            Stripe.setPublishableKey('pk_test_ossp9HlRPSnMdE5Rq4eNxCHt');
        </script>




    </body>
</html>
