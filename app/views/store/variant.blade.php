<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')
            

            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                <ul class="breadcrumb">
                    <li><a href="/browse">Shop</a> <span class="divider">/</span></li>
                    <li><a href="/browse/style/{{ $product->style->name }}">{{ $product->style->name }}</a> <span class="divider">/</span></li>
                    <li class="active">{{ $product->name }}</li>
                </ul>

                <div class="row">
                
                    <!-- Begin product image -->
                    <div class="product-image span5">
                        <img src="{{ Image::getLargePhoto($product->photos->first()) }}" />
                    </div>
                    <!-- End product image -->

                    <div class="product-details span6">
                        <div class="brand">{{ $product->brand->name }}</div>
                        <h2 class="title">{{ $product->name }}</h2>
                        <div class="price">${{ $product->price }}</div>
                        <div class="retail-price">Retail price ${{ $product->retail_price }}</div>

                        <div id="product-id" data-productid="{{ $product->id }}"></div>
                        <div id="variant-id" data-variantid="{{ $variant->id }}"></div>

                        <ul class="unstyled">
                            <li>
                                <span class="type">Size: </span>
                                <span class="value">{{ $variant->size->name }}</span>
                            </li>
                            <li>
                                <span class="type">Color: </span>
                                <span class="value">{{ $variant->color->name }}</span>
                            </li>
                        </ul>

                        <ul class="unstyled">
                            <li>
                                <span class="type">Condition: {{ $variant->condition->name }}</span>
                                <span class="details">{{ $variant->condition->description }}</span>
                            </li>
                        </ul>

                        <div id="add-to-cart" class="btn btn-large btn-add-cart">Add to Cart</div>
                    </div>

                </div>

                <div class="row">
                    <div class="product-details2 span12">
                        <h4>Description</h4>
                        <div class="description">{{ $product->brand->description }}</div>
                        <div class="description">{{ $product->description }}</div>
                    </div>
                </div>





            </div>
            <!-- End Main -->   
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script src="/js/variant.js"></script>




    </body>
</html>
