<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('store.partials.css')


    </head>
    <body>


        <!-- Start Wrap -->
        <div id="wrap">
            @include('store.partials.header')
            

            <!-- Start Main -->
            <div class="main-wrapper clearfix">
                
                <h1>Order cannot be made</h1>

                <div class="row main">

                    <div class="span12">
                        <h3>Items get grabbed really fast around here!</h3>
                        
                        <div class="alert" id="alert-msg">
                            <a class="close" data-dismiss="alert" href="#">&times;</a>
                            One or more items have been removed from your shopping cart due to insufficient quantity in stock.
                        </div>

                        <p>Please review your shopping cart again or look for more items in our store.</p>
                        <br />
                        <a href="/browse" class="btn">Continue Shopping</a>
                        <a href="/cart" class="btn">View Cart</a>
                    </div>
                </div>
                
            </div>
            <!-- End Main -->
            
        </div>
        <!-- End Wrap -->


        @include('store.partials.footer')





        <!-- Javascript -->
        @include('store.partials.js')
        <script type="text/javascript" src="/js/checkout.js"></script>




    </body>
</html>
