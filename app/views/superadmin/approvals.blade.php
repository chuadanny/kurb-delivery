<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('superadmin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Approvals</h1>
                        <div class="heading-toolbar">
                            <a href="#" id="approve-btn" class="btn btn-success pull-right">Approve</a>
                            <a href="#" id="edit-btn" class="btn btn-info pull-right">Edit</a>
                            <a href="#" id="reject-btn" class="btn btn-danger pull-right">Reject</a>
                            <div class="label-as-hot pull-right">
                                <label class="checkbox">
                                    <input type="checkbox" id="label-hot">
                                    Label as "Hot"
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="content">

                        <input type="hidden" id="variant-id" value="{{ $variant->id }}" />

                        <div class="row">
                            <div class="span6">
                                <h3>Product</h3>
                                <table class="table table-striped info-table">
                                    <tr>
                                        <td class="type">Variant ID</td>
                                        <td class="value">{{ $variant->id }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Brand</td>
                                        <td class="value">{{ $variant->product->brand->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Product</td>
                                        <td class="value">{{ $variant->product->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Price</td>
                                        <td class="value">${{ $variant->price }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Gender</td>
                                        <td class="value">{{ $variant->product->gender->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Category</td>
                                        <td class="value">{{ $variant->product->category->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Style</td>
                                        <td class="value">{{ $variant->product->style->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Color</td>
                                        <td class="value">{{ $variant->color->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Size</td>
                                        <td class="value">{{ $variant->size->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Material</td>
                                        <td class="value">{{ $variant->product->material->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">SKU</td>
                                        <td class="value">{{ $variant->sku }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Added</td>
                                        <td class="value">{{ $variant->created_at }}</td>
                                    </tr>
                                </table>

                                <h3>Shop</h3>
                                <table class="table table-striped info-table">
                                    <tr>
                                        <td class="type">Shop</td>
                                        <td class="value">{{ $shop->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Owner</td>
                                        <td class="value">{{ $shop->user->first_name }} {{ $shop->user->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Phone</td>
                                        <td class="value">{{ $shop->user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Email</td>
                                        <td class="value">{{ $shop->user->email }}</td>
                                    </tr>
                                </table>

                                <h3>EXIF</h3>
                                <table class="table table-striped info-table">
                                    <tr>
                                        <td class="type">Aperture</td>
                                        <td class="value">{{ $exif->getAperture() }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Shutter speed</td>
                                        <td class="value">{{ $exif->getExposure() }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">ISO</td>
                                        <td class="value">{{ $exif->getIso() }}</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Focal length</td>
                                        <td class="value">{{ $exif->getFocalLength() }} mm</td>
                                    </tr>
                                    <tr>
                                        <td class="type">Camera</td>
                                        <td class="value">{{ $exif->getCamera() }}</td>
                                    </tr>
                                </table>


                            </div>

                            <div class="span5">
                                <h3>Processed</h3>
                                <a href="{{ Image::getFullPath($photo) }}" class="MagicZoom" >
                                    <img src="{{ Image::getFullPath($photo) }}" />
                                </a>
                            </div>

                            <div class="span5">
                                <h3>Unprocessed</h3>
                                <a href="{{ Image::getFullPath($photo) }}" class="MagicZoom" >
                                    <img src="{{ Image::getFullPath($photo) }}" />
                                </a>
                            </div>

                        </div>
                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')




        <!-- Modals -->
        <div id="reject-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Reject Entry</h4>
            </div>
            <div class="modal-body">
                <label for="reject-reason">Reason for rejecting</label>
                <select id="reject-reason">
                    <option>Photo is not in focus.</option>
                    <option>Photo is overexposed.</option>
                    <option>Photo is underexposed.</option>
                    <option>Product is not centered in photo.</option>
                    <option>Photo was not well taken.</option>
                </select>
                <br />
                <textarea id="optional-reason" class="input-xlarge" placeholder="Optional message"></textarea>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                <a href="#" id="submit-reject" class="btn btn-primary">Submit Rejection</a>
            </div>
        </div>



        <div id="edit-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Edit Entry</h4>
            </div>
            <div class="modal-body">
                <form id="edit-product-form" method="post" action="/superadmin-api/edit-variant">
                    <h3>Photo</h3>
                    <div class="row">
                        <div class="span3">
                            <label for="original-photo">Original Photo</label>
                            <a href="{{ Image::getFullPath($photo) }}" class="btn" download>Download</a>
                        </div>
                        <div class="span2">
                            <label for="product-photo">Replacement Photo</label>
                            <input type="file" id="product-photo" name="product-photo">
                        </div>
                    </div>


                    <input type="hidden" name="variant-id" value="{{ $variant->id }}">


                    <h3>Product Details</h3>
                    <div class="row">
                        <div class="span3">
                            <label for="product-color">Color</label>
                            <select name="product-color">
                                @foreach(Color::all() as $color)
                                    <option value="{{ $color->id }}" 
                                        @if($color->id == $variant->color_id) 
                                            {{ 'selected' }}
                                        @endif
                                        >
                                        {{ $color->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="span3">
                            <label for="product-category">Category</label>
                            <select name="product-category">
                                @foreach(Category::all() as $category)
                                    <option value="{{ $category->id }}" 
                                        @if($category->id == $variant->product->category->id) 
                                            {{ 'selected' }}
                                        @endif
                                        >
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="span3">
                            <label for="product-gender">Gender</label>
                            <select name="product-gender">
                                @foreach(Gender::all() as $gender)
                                    <option value="{{ $gender->id }}" 
                                        @if($gender->id == $variant->gender_id) 
                                            {{ 'selected' }}
                                        @endif
                                        >
                                        {{ $gender->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="span3">
                            <label for="product-style">Style</label>
                            <select name="product-style">
                                @foreach(Style::all() as $style)
                                    <option value="{{ $style->id }}" 
                                        @if($style->id == $variant->product->style->id) 
                                            {{ 'selected' }}
                                        @endif
                                        >
                                        {{ $style->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="span3">
                            <label for="product-material">Material</label>
                            <select name="product-material">
                                @foreach(Material::all() as $material)
                                    <option value="{{ $material->id }}" 
                                        @if($material->id == $variant->product->material_id) 
                                            {{ 'selected' }}
                                        @endif
                                        >
                                        {{ $material->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </form>

            </div>
            <div class="modal-footer">
                <a id="close-modal-edit-product" href="#" class="btn" data-dismiss="modal">Cancel</a>
                <a id="submit-edited-product" class="btn btn-primary" data-loading-text="Saving...">Apply Changes</a>
            </div>
        </div>




        <!-- Javascript -->
        @include('admin.partials.js')
        <script src="/js/superadmin.js"></script>









    </body>
</html>
