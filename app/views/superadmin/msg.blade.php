<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('superadmin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Approvals</h1>
                        <div class="heading-toolbar">
                            <a href="#" id="approve-btn" class="btn btn-success pull-right">Approve</a>
                            <a href="#" id="reject-btn" class="btn btn-danger pull-right">Reject</a>
                        </div>
                    </div>
                    <div class="content">

                        <h2>{{ $msg }}</h2>

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Javascript -->
        @include('admin.partials.js')
        <script src="/js/superadmin.js"></script>









    </body>
</html>
