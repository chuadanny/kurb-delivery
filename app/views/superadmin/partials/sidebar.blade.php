                <!-- Start Sidebar -->
                <div class="sidebar">
                    <nav>
                        <h2>Your Store</h2>
                        <ul>
                            <li @if($currentPage == 'home') class="selected" @endif><a href="/superadmin"><i class="icon-home icon-white"></i> Dashboard</a></li>
                            <li @if($currentPage == 'approvals') class="selected" @endif><a href="/superadmin/approvals"><i class="icon-barcode"></i> Approvals</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- End Sidebar -->