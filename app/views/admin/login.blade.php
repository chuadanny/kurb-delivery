<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body class="admin-login">

        <div class="login-box-wrapper clearfix">

            <img src="/img/template/logo-dark.png" class="logo" />
            <h3>Ambassador Store Management</h3>

            <div class="row">
                <div class="login-box span4">
                    @if(!Session::has('error'))
                        <h4>Welcome back!</h4>
                    @else
                        <h4>Please try again</h4>
                        <div class="alert">{{ Session::get('error') }}</div>
                    @endif
                    <br />
                    <form method="POST" action="/admin/login">
                        <input type="text" name="email" placeholder="Email" />
                        <input type="password" name="password" placeholder="Password" />
                        <input type="submit" name="submit" class="btn btn-block btn-large btn-primary" />
                    </form>
                </div>
            </div>
        </div>





        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
