<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Quick Look</h1>
                    </div>
                    <div class="content">
                        
                        <div class="row">
                            <div class="span5 summary-box">
                                <div class="type">This Week</div>
                                <div class="value">${{ $shop->salesThisWeek() }}</div>
                                <div class="subvalue">{{ $shop->ordersThisWeek() }} orders</div>
                            </div>
                            <div class="span5 summary-box">
                                <div class="type">This Month</div>
                                <div class="value">${{ $shop->salesThisMonth() }}</div>
                                <div class="subvalue">{{ $shop->ordersThisMonth() }} orders</div>
                            </div>
                            <div class="span5 summary-box">
                                <div class="type">Open Orders</div>
                                <div class="value-large">{{ $shop->openOrders() }}</div>
                            </div>
                        </div>


                        <h3>Hurray! This stuff has sold - Now quickly ship!</h3>
                        <table class="table inventory-table" id="orders-table">
                            <thead>
                                <tr>
                                    <th class="select"></th>
                                    <th class="">Order</th>
                                    <th class="">Date</th>
                                    <th class="">Customer</th>
                                    <th class="">Payment Status</th>
                                    <th class="">Fufillment Status</th>
                                    <th class="">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($orders))
                                    @foreach($orders as $order_id => $order)
                                        <tr>
                                            <td><input type="checkbox"></input></td>
                                            <td>
                                                <a href="/admin/order/{{ $order_id }}">#{{ $order_id }}</a>
                                            </td>
                                            <td>
                                                {{ date('F j Y, g:ia', strtotime($order->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $order->first_name }} {{ $order->last_name }}
                                            </td>
                                            <td><span class="label">{{ ucfirst($order->transaction->status) }}</span></td>
                                            <td><span class="label">{{ ucfirst($order->vendorFulfillment()) }}</span></td>
                                            <td>${{ $order->subtotal }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>

                        


                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
