<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Products / Add Product</h1>

                        <div class="heading-toolbar">
                            <a href="#" id="product-add-form-submit" class="btn btn-success pull-right">Save</a>
                            <a href="/admin/products" class="btn pull-right">Cancel</a>
                        </div>
                    </div>

                    <div class="content">

                        <form id="product-add-form" method="POST" enctype="multipart/form-data" action="/admin/product-add">




                             <!-- Photos -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Photo</h4>
                                    <p>Upload images of the product.</p>
                                </div>

                                <div class="span7 offset1">
                                    <div class="row">
                                        <div class="span3">
                                            <input type="file" id="product-photo" name="product-photo">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        
                            <!-- Product Details -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Product Details</h4>
                                    <p>Write a name and description, and provide a type and vendor to categorize this product.</p>
                                </div>

                                <div class="span7 offset1">
                                    <label for="product-name">Name</label>
                                    <input type="text" name="product-name" class="input-xxlarge">

                                    <label for="product-name">Description</label>
                                    <textarea name="product-description" rows="5" class="input-xxlarge"></textarea>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-gender">Gender</label>
                                            <select name="product-gender">
                                                @foreach(Gender::all() as $gender)
                                                    <option value="{{ $gender->id }}">
                                                        {{ $gender->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <label for="product-category">Category</label>
                                            <select name="product-category">
                                                @foreach(Category::all() as $category)
                                                    <option value="{{ $category->id }}">
                                                        {{ $category->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-brand">Brand</label>
                                            <select name="product-brand">
                                                @foreach($brands as $brand)
                                                    <option value="{{ $brand->id }}">
                                                        {{ $brand->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <label for="product-style">Style</label>
                                            <select name="product-style">
                                                @foreach(Style::all() as $style)
                                                    <option value="{{ $style->id }}">
                                                        {{ $style->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <!-- Inventory and Variants -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Inventory and Variants</h4>
                                    <p>Manage inventory, and configure the options for selling this product.</p>
                                </div>

                                <div class="span7 offset1">
                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-price">Price</label>
                                            <input type="text" id="product-price" name="product-price">
                                        </div>
                                        <div class="span3">
                                            <label for="product-retail-price">Retail Price</label>
                                            <input type="text" id="product-retail-price" name="product-retail-price">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-sku">SKU</label>
                                            <input type="text" id="product-sku" name="product-sku">
                                        </div>
                                        <div class="span3">
                                            <label for="product-barcode">Internal SKU</label>
                                            <input type="text" id="product-internal-sku" name="product-internal-sku">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-size">Size</label>
                                            <select name="product-size">
                                                @foreach(Size::all() as $size)
                                                    <option value="{{ $size->id }}">
                                                        {{ $size->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <label for="product-color">Color</label>
                                            <select name="product-color">
                                                @foreach(Color::all() as $color)
                                                    <option value="{{ $color->id }}">
                                                        {{ $color->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-condition">Condition</label>
                                            <select name="product-condition">
                                                @foreach(Condition::all() as $condition)
                                                    <option value="{{ $condition->id }}">
                                                        {{ $condition->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="span3">
                                            <label for="product-weight">Weight <span class="hint">(lb)</span></label>
                                            <input type="text" id="product-weight" name="product-weight">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-material">Material</label>
                                            <select name="product-material">
                                                @foreach(Material::all() as $material)
                                                    <option value="{{ $material->id }}">
                                                        {{ $material->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>


                             <!-- Visibility -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Visibility</h4>
                                    <p>Control if this product can be viewed on your storefront. Your photo of this product must still be approved before the product can appear on the website.</p>
                                </div>

                                <div class="span7 offset1">
                                    <div class="row">
                                        <div class="span5">
                                            <label class="radio">
                                                <input type="radio" name="product-visibility" id="product-visibility" value="1" checked>
                                                Visible
                                                <span class="hint">(Everybody can view this product)</span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="product-visibility" id="product-visibility" value="0">
                                                Hidden
                                                <span class="hint">(Nobody can view this product)</span>
                                            </label>
                                    </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Javascript -->
        @include('admin.partials.js')










    </body>
</html>
