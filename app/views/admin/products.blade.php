<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Products</h1>
                        <div class="heading-toolbar">
                            <a href="/admin/product-add" class="btn btn-success pull-right">Add product</a>
                        </div>
                    </div>


                                
                    <div class="content">

                        <div class="toolbar">
                            <form id="filter-form" method="GET" action="/admin/products">
                                <div class="btn-group search-box">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Filter products
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <label for="filter-visibility">
                                            Visibility
                                        </label>
                                        {{ @Form::select('filter-visibility', 
                                            array('-1'=>'Show all','1'=>'Visible', '0'=>'Hidden'),
                                            Input::get('filter-visibility')) }}


                                        <label for="filter-category">
                                            Categories
                                        </label>
                                        {{ @Form::select('filter-category', 
                                           ['0'=>'Show all'] + Category::all()->lists('name','id'),
                                            Input::get('filter-category')) }}


                                        <label for="filter-brand">
                                            Brands
                                        </label>
                                        {{ @Form::select('filter-brand', 
                                           ['0'=>'Show all'] + Brand::all()->lists('name','id'),
                                           Input::get('filter-brand')) }}

                                        <a id="filters-apply-btn" class="btn btn-block btn-primary">Apply Filter</a>
                                    </ul>

                                    <input class="span8 search-box" id="queryString" name="query" type="text" placeholder="Start typing a product's name" value="{{ Input::get('query') }}">
                                </div>
                            </form>
                        </div>

                        <table class="table inventory-table">
                            <thead>
                                <tr>
                                    <th class="select"></th>
                                    <th class="product-img"></th>
                                    <th class="name is-sortable">Product</th>
                                    <th class="inventory is-sortable">Inventory</th>
                                    <th class="type is-sortable">Type</th>
                                    <th class="brand is-sortable">Brand</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($variants))
                                    @foreach($variants as $variant)
                                        <tr>
                                            <td><input type="checkbox"></input></td>
                                            <td>
                                                <img src="{{ Image::getSmallPhoto($variant->photos->first()) }}" class="product-img" />
                                            </td>
                                            <td class="name">
                                                <a href="/admin/variant/{{ $variant->id }}">
                                                    {{ $variant->product->name }}
                                                </a>
                                            </td>
                                            <td>{{ $variant->quantity }}</td>
                                            <td>{{ $variant->product->category->name }}</td>
                                            <td>{{ $variant->product->brand->name }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>


                        @if(isset($variants))
                            {{ $variants->appends($oldInput)->links() }}
                        @endif

                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
