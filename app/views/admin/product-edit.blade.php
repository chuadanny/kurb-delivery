<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Products / Edit Product</h1>

                        <div class="heading-toolbar">
                            <a href="#" class="btn btn-success pull-right submit-form-btn">Save</a>
                            <a href="/admin/products" class="btn pull-right">Cancel</a>
                        </div>
                    </div>

                    <div class="content">

                        @if(Session::has('msg'))
                            <div class="alert alert-success hidden" id="alert-msg">
                                {{ Session::get('msg') }}
                            </div>
                        @endif

                        <form id="product-edit-form" method="POST" enctype="multipart/form-data" action="/admin/variant-edit">

                        
                            <!-- Product Details -->
                            <div class="row section">

                                <input type="hidden" name="variant-id" id="variant-id" value="{{ $variant->id }}">

                                <div class="span4">
                                    <h4>Product Details</h4>
                                    <p>Write a name and description, and provide a type and vendor to categorize this product.</p>
                                    <img src="{{ Image::getMediumPhoto($variant->photos->first()) }}" />
                                </div>

                                <div class="span7 offset1">
                                    <label for="product-name">Name</label>
                                    <input type="text" name="product-name" class="input-xxlarge" value="{{ $product->name }}">

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-gender">Gender</label>
                                            {{ @Form::select('product-gender', Gender::all()->lists('name', 'id'), $product->gender->id) }}
                                            
                                        </div>
                                        <div class="span3">
                                            <label for="product-category">Category</label>
                                            {{ @Form::select('product-category', Category::all()->lists('name', 'id'), $product->category->id) }}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-brand">Brand</label>
                                            {{ @Form::select('product-brand', Brand::all()->lists('name', 'id'), $product->brand->id) }}
                                        </div>
                                        <div class="span3">
                                            <label for="product-style">Style</label>
                                            {{ @Form::select('product-style', Style::all()->lists('name', 'id'), $product->style->id) }}
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <!-- Inventory and Variants -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Inventory and Variants</h4>
                                    <p>Manage inventory, and configure the options for selling this product.</p>
                                </div>

                                <div class="span7 offset1">
                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-price">Price</label>
                                            <input type="text" id="product-price" name="product-price" value="{{ $variant->price }}">
                                        </div>
                                        <div class="span3">
                                            <label for="product-retail-price">Retail Price</label>
                                            <input type="text" id="product-retail-price" name="product-retail-price" value="{{ $variant->retail_price }}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-sku">SKU</label>
                                            <input type="text" id="product-sku" name="product-sku" value="{{ $variant->sku }}">
                                        </div>
                                        <div class="span3">
                                            <label for="product-internal-sky">Internal SKU</label>
                                            <input type="text" id="product-internal-sku" name="product-internal-sku" value="{{ $variant->internal_sku }}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-size">Size</label>
                                            {{ @Form::select('product-size', Size::all()->lists('name', 'id'), $variant->size->id) }}
                                        </div>
                                        <div class="span3">
                                            <label for="product-color">Color</label>
                                            {{ @Form::select('product-color', Color::all()->lists('name', 'id'), $variant->color->id) }}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-condition">Condition</label>
                                            {{ @Form::select('product-condition', Condition::all()->lists('name', 'id'), $variant->condition->id) }}
                                        </div>
                                        <div class="span3">
                                            <label for="product-weight">Weight <span class="hint">(lb)</span></label>
                                            <input type="text" id="product-weight" name="product-weight" value="{{ $variant->weight }}">
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <!-- Photos -->


                            <!-- Visibility -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Visibility</h4>
                                    <p>Control if this product can be viewed on your storefront. Your photo of this product must still be approved before the product can appear on the website.</p>
                                </div>

                                <div class="span7 offset1">
                                    <div class="row">
                                        <div class="span5">
                                            <label class="radio">
                                                <input type="radio" name="product-visibility" id="product-visibility" value="1" 
                                                @if($variant->is_visible)
                                                    {{ 'checked' }}
                                                @endif>
                                                Visible
                                                <span class="hint">(Everybody can view this product)</span>
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="product-visibility" id="product-visibility" value="0"
                                                @if(!$variant->is_visible)
                                                    {{ 'checked' }}
                                                @endif>
                                                Hidden
                                                <span class="hint">(Nobody can view this product)</span>
                                            </label>
                                    </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Delete -->
                            <div class="row-fluid section">
                                <div class="span12">
                                    <a id="product-delete" class="btn btn-danger pull-left">Delete this product</a>
                                    <a class="btn btn-success pull-right submit-form-btn">Save</a>
                                </div>
                            </div>

                        </form>
                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')










        <!-- Modals -->
        <div id="product-delete-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Delete {{ $variant->product->name }}?</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete <strong>{{ $variant->product->name }}</strong>? This is a permanent action and cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                <a href="#" id="confirm-product-delete" class="btn btn-danger">Delete</a>
            </div>
        </div>










        <!-- Javascript -->
        @include('admin.partials.js')










    </body>
</html>
