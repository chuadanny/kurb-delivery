<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Orders</h1>
                        <div class="heading-toolbar">
                            <a href="/admin/product-add" class="btn btn-success pull-right">Add product</a>
                        </div>
                    </div>


                                
                    <div class="content">

                        <div class="toolbar">
                            <form id="filter-form" method="GET" action="/admin/orders">
                                <div class="btn-group search-box">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Filter orders
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <label for="filter-payment">
                                            Payment Status
                                        </label>
                                        {{ @Form::select('filter-payment', 
                                            array('0'=>'Show all', 'paid'=>'Paid', 'refunded'=>'Refunded'),
                                            Input::get('filter-payment')) }}


                                        <label for="filter-fulfillment">
                                            Fulfillment Status
                                        </label>
                                        {{ @Form::select('filter-fulfillment', 
                                           ['0'=>'Show all', 'pending'=>'Not fulfilled', 'fulfilled'=>'Fulfilled'],
                                            Input::get('filter-fulfillment')) }}

                                        <a id="filters-apply-btn" class="btn btn-block btn-primary">Apply Filter</a>
                                    </ul>

                                    <input class="span8 search-box" id="queryString" name="query" type="text" placeholder="Start typing an order number or a customer's name" value="{{ Input::get('query') }}">
                                </div>
                            </form>
                        </div>

                        <table class="table inventory-table" id="orders-table">
                            <thead>
                                <tr>
                                    <th class="select"></th>
                                    <th class="is-sortable">Order</th>
                                    <th class=" is-sortable">Date</th>
                                    <th class=" is-sortable">Customer</th>
                                    <th class=" is-sortable">Payment Status</th>
                                    <th class=" is-sortable">Fufillment Status</th>
                                    <th class=" is-sortable">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($orders))
                                    @foreach($orders as $order_id => $order)
                                        <tr>
                                            <td><input type="checkbox"></input></td>
                                            <td>
                                                <a href="/admin/order/{{ $order_id }}">#{{ $order_id }}</a>
                                            </td>
                                            <td>
                                                {{ date('F j Y, g:ia', strtotime($order->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $order->first_name }} {{ $order->last_name }}
                                            </td>
                                            <td><span class="label">{{ ucfirst($order->transaction->status) }}</span></td>
                                            <td><span class="label">{{ ucfirst($order->vendorFulfillment()) }}</span></td>
                                            <td>${{ $order->subtotal }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>


                        @if(isset($variants))
                            {{ $variants->appends($oldInput)->links() }}
                        @endif

                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
