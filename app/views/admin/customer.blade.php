<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Customers / {{ $customer->first_name }} {{ $customer->last_name }}</h1>
                        <div class="heading-toolbar">
                            <a id="customer-update-btn" class="btn btn-success pull-right">Save changes</a>
                        </div>
                    </div>


                                
                    <div class="content">

                        <div class="alert alert-success hidden" id="alert-msg">
                            <a class="close" data-dismiss="alert" href="#">&times;</a>
                        </div>

                        <div class="row">

                            <div class="span12">

                                <h3>Customer Details</h3>
                                <table class="table inventory-table" id="orders-table">
                                    <input id="customer-id" type="hidden" value="{{ $customer->id }}">
                                    <thead>
                                        <tr>
                                            <th class="select"></th>
                                            <th class="is-sortable">Order</th>
                                            <th class=" is-sortable">Date</th>
                                            <th class=" is-sortable">Payment Status</th>
                                            <th class=" is-sortable">Fufillment Status</th>
                                            <th class=" is-sortable">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($orders))
                                            @foreach($orders as $order_id => $order)
                                                <tr>
                                                    <td><input type="checkbox"></input></td>
                                                    <td>
                                                        <a href="/admin/order/{{ $order_id }}">#{{ $order_id }}</a>
                                                    </td>
                                                    <td>
                                                        {{ date('F j Y, g:ia', strtotime($order->created_at)) }}
                                                    </td>
                                                    <td><span class="label">{{ ucfirst($order->transaction->status) }}</span></td>
                                                    <td><span class="label">{{ ucfirst($order->vendorFulfillment()) }}</span></td>
                                                    <td>${{ $order->subtotal }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>


                                <br />


                                <h3>Customer Notes</h3>
                                <textarea id="customer-notes" class="span12" placeholder="Add a note to this customer...">{{ $notes }}</textarea>








                            </div>






                            <div class="span4">
                                <div class="box">
                                    <div class="box-header">
                                        <h5>Address</h5>
                                    </div>
                                    <div class="box-contents">
                                        <p>
                                            <strong>{{ $customer->first_name }} {{ $customer->last_name }}</strong>
                                        </p>
                                        <p>
                                            {{ $customer->address_1 }}
                                            @if($customer->address_2) {{ $customer->address_2 }} @endif <br/>
                                            {{ $customer->city }}, {{ $customer->state }} {{ $customer->postal }}
                                        </p>
                                        <p>
                                            <i class="icon-phone"></i> {{ $customer->phone() }} <br />
                                            <i class="icon-envelope"></i> {{ $customer->email }}
                                        </p>
                                    </div>
                                </div>

                                <div class="box">
                                    <div class="box-header">
                                        <h5>Details</h5>
                                    </div>
                                    <div class="box-contents">
                                        <ul class="unstyled">
                                            <li><i class="icon-phone"></i> ${{ $customer->totalSpentInShop($shop->id) }} spent</li>
                                            <li><i class="icon-envelope"></i> 
                                                {{ $customer->productOrderedCountInShop($shop->id) }} 
                                                @if($customer->productOrderedCountInShop($shop->id) > 1)
                                                    products ordered
                                                @else
                                                    product ordered
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
  
                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Modals -->
        <div id="fulfill-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Fulfill Entire Order</h4>
            </div>
            <div class="modal-body">
                <label for="tracking-number">Tracking number</label>
                <input id="tracking-number" type="text" placeholder="Enter the tracking number...">
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                <a href="#" id="submit-btn" class="btn btn-primary">Submit Rejection</a>
            </div>
        </div>










        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
