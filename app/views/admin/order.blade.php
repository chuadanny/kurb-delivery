<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Orders / #{{ $order->id }}</h1>
                        <div class="heading-toolbar">
                            <a id="order-update-btn" class="btn btn-success pull-right">Save changes</a>
                        </div>
                    </div>


                                
                    <div class="content">

                        <div class="alert alert-success hidden" id="alert-msg">
                            <a class="close" data-dismiss="alert" href="#">&times;</a>
                        </div>

                        <div class="row">

                            <div class="span12">

                                <h3>Order Details</h3>
                                <div class="order-toolbar">
                                    @if($order->vendorFulfillment() == 'Fulfilled')
                                        <a class="btn disabled pull-left">All line items fulfilled</a>
                                    @else
                                        <a id="fulfill-order-btn" class="btn btn-primary pull-left">
                                            Fulfill {{ count($order->ordervariants) }} line @if(count($order->ordervariants) > 1) {{ 'items' }} @else {{ 'item' }} @endif
                                        </a>
                                    @endif
                                </div>
                                <table class="order-table">

                                    <input id="order-id" type="hidden" value="{{ $order->id }}">

                                    <!-- Line Items -->
                                    <tbody class="order-contents">
                                        @foreach($order->ordervariants as $ordervariant)
                                            <tr>
                                                <td class="checkbox is-selected">
                                                    @if($ordervariant->fulfillment == 'fulfilled')
                                                        <i class="icon-truck" title="Line item is fulfilled"></i>
                                                    @else 
                                                        <input type="checkbox" checked disabled></input>
                                                    @endif
                                                </td>
                                                <td class="description">
                                                    <a href="/admin/variant/{{ $ordervariant->variant->id }}">
                                                        {{ $ordervariant->variant->product->name }}
                                                    <a/>

                                                    @if($ordervariant->fulfillment == 'fulfilled')
                                                        <span class="label label-success">Fulfilled</span>
                                                    @endif

                                                    <span class="price-quantity">
                                                        <span class="price">${{ $ordervariant->variant->price }}</span> 
                                                        <span class="item-multiplier">x</span> 
                                                        <span class="quantity">{{ $ordervariant->quantity }}</span>
                                                    </span>
                                                </td>
                                                <td class="subtotal">
                                                    ${{ $ordervariant->variant->price }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    <!-- Order Summary -->
                                    <tbody class="order-summary">
                                        <tr>
                                            <td class="type" colspan="2">Subtotal:</td>
                                            <td class="value">${{ $order->subtotal }}</td>
                                        </tr>
                                        <tr>
                                            <td class="type" colspan="2">Shipping:</td>
                                            <td class="value">${{ $order->shipping }}</td>
                                        </tr>
                                        <tr>
                                            <td class="type" colspan="2">Sales Tax:</td>
                                            <td class="value">${{ $order->tax }}</td>
                                        </tr>
                                        <tr>
                                            <td class="type" colspan="2">Total:</td>
                                            <td class="value">${{ $order->total() }}</td>
                                        </tr>
                                    </tbody>

                                    <!-- Order Transaction -->
                                    <tbody class="order-transaction">
                                        <tr>
                                            <td class="type" colspan="2">Payment:</td>
                                            <td class="value">${{ $order->transaction->amount }}</td>
                                        </tr>
                                    </tbody>

                                </table>

                                <br />


                                @if($order->vendorFulfillment() == 'Fulfilled')
                                    <h3>Tracking Number</h3>
                                    <input type="text" value="{{ $order->orderVariants->first()->tracking_number }}" disabled></input>
                                @endif


                                <h3>Order Notes</h3>
                                <textarea id="order-notes" class="span12" placeholder="Add a note to this order...">{{ $order->notes }}</textarea>








                            </div>






                            <div class="span4">
                                <div class="box">
                                    <div class="box-header">
                                        <h5>Shipping Address</h5>
                                    </div>
                                    <div class="box-contents">
                                        <p>
                                            <strong>{{ $order->first_name }} {{ $order->last_name }}</strong>
                                        </p>
                                        <p>
                                            {{ $order->address_1 }}
                                            @if($order->address_2) {{ $order->address_2 }} @endif <br/>
                                            {{ $order->city }}, {{ $order->state }} {{ $order->postal }}
                                        </p>
                                        <p>
                                            <i class="icon-phone"></i> {{ $order->phone() }} <br />
                                            <i class="icon-envelope"></i> {{ $order->email }}
                                        </p>
                                    </div>
                                </div>


                                <div class="box">
                                    <div class="box-header">
                                        <h5>Billing Address</h5>
                                    </div>
                                    <div class="box-contents">
                                        <p>
                                            <strong>{{ $order->first_name }} {{ $order->last_name }}</strong>
                                        </p>
                                        <p>
                                            {{ $order->address_1 }}
                                            @if($order->address_2) {{ $order->address_2 }} @endif <br/>
                                            {{ $order->city }}, {{ $order->state }} {{ $order->postal }}
                                        </p>
                                        <p>
                                            <i class="icon-phone"></i> {{ $order->phone() }} <br />
                                            <i class="icon-envelope"></i> {{ $order->email }} <br />
                                            <i class="icon-user"></i> <a href="/admin/customer/{{ $order->customer->id }}">View customer profile</a>
                                        </p>
                                    </div>
                                </div>
                            </div>


                        </div>
  
                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Modals -->
        <div id="fulfill-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Fulfill Entire Order</h4>
            </div>
            <div class="modal-body">
                <label for="tracking-number">Tracking number</label>
                <input id="tracking-number" type="text" placeholder="Enter the tracking number...">
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                <a href="#" id="submit-btn" class="btn btn-primary">Complete Fulfillment</a>
            </div>
        </div>










        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
