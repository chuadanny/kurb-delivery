            <div class="header-wrapper">
                <div class="header">
                    <img src="/img/template/logo.png" class="logo" />

                    <div class="user-bar">
                                

                        <div class="dropdown dropdown-toggle">
                            <a href="#" data-toggle="dropdown">
                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li><a tabindex="-1" href="/admin/logout">Log out</a></li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>