                <!-- Start Sidebar -->
                <div class="sidebar">
                    <nav>
                        <h2>Your Store</h2>
                        <ul>
                            <li @if($currentPage == 'home') class="selected" @endif><a href="/admin"><i class="icon-home"></i> Quick Look</a></li>
                            <li @if($currentPage == 'orders') class="selected" @endif><a href="/admin/orders"><i class="icon-barcode"></i> Orders</a></li>
                            <li @if($currentPage == 'customers') class="selected" @endif><a href="/admin/customers"><i class="icon-user"></i> Customers</a></li>
                            <li @if($currentPage == 'products') class="selected" @endif><a href="/admin/products"><i class="icon-tags"></i> Products</a></li>
                        </ul>
                        <h2>Settings</h2>
                        <ul>
                            <li @if($currentPage == 'store') class="selected" @endif><a href="#"><i class="icon-home"></i> Store</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- End Sidebar -->