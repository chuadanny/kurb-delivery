<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Products / {{ $product->name }}</h1>

                        <div class="heading-toolbar">
                        </div>
                    </div>

                    <div class="content">

                        @if(Session::has('msg'))
                            <div class="alert alert-success fade">
                                <a class="close" data-dismiss="alert" href="#">&times;</a>
                                {{ Session::get('msg') }}
                            </div>
                        @endif

                        <div class="alert alter-success">
                            <a class="close" data-dismiss="alert" href="#">&times;</a>
                            This product has been sold.
                        </div>

                        <form id="product-edit-form" method="POST" enctype="multipart/form-data" action="/admin/variant-edit">

                        
                            <!-- Product Details -->
                            <div class="row section">

                                <input type="hidden" name="variant-id" id="variant-id" value="{{ $variant->id }}">

                                <div class="span4">
                                    <h4>Product Details</h4>
                                    <p>Write a name and description, and provide a type and vendor to categorize this product.</p>
                                    <img src="{{ Image::getMediumPhoto($variant->photos->first()) }}" />
                                </div>

                                <div class="span7 offset1">
                                    <label for="product-name">Name</label>
                                    <input type="text" name="product-name" class="input-xxlarge" value="{{ $product->name }}" disabled>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-gender">Gender</label>
                                            <input type="text" name="product-gender" value="{{ $product->gender->name }}" disabled>
                                            
                                        </div>
                                        <div class="span3">
                                            <label for="product-category">Category</label>
                                            <input type="text" name="product-category" value="{{ $product->category->name }}" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-brand">Brand</label>
                                            <input type="text" name="product-brand" value="{{ $product->brand->name }}" disabled>
                                        </div>
                                        <div class="span3">
                                            <label for="product-style">Style</label>
                                            <input type="text" name="product-style" value="{{ $product->style->name }}" disabled>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <!-- Inventory and Variants -->
                            <div class="row section">
                                <div class="span4">
                                    <h4>Inventory and Variants</h4>
                                    <p>Manage inventory, and configure the options for selling this product.</p>
                                </div>

                                <div class="span7 offset1">
                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-price">Price</label>
                                            <input type="text" id="product-price" name="product-price" value="{{ $variant->price }}" disabled>
                                        </div>
                                        <div class="span3">
                                            <label for="product-retail-price">Retail Price</label>
                                            <input type="text" id="product-retail-price" name="product-retail-price" value="{{ $variant->retail_price }}" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-sku">SKU</label>
                                            <input type="text" id="product-sku" name="product-sku" value="{{ $variant->sku }}" disabled>
                                        </div>
                                        <div class="span3">
                                            <label for="product-internal-sku">Internal SKU</label>
                                            <input type="text" id="product-internal-sku" name="product-internal-sku" value="{{ $variant->interal_sku }}" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-size">Size</label>
                                            <input type="text" name="product-size" value="{{ $variant->size->name }}" disabled>
                                        </div>
                                        <div class="span3">
                                            <label for="product-color">Color</label>
                                            <input type="text" name="product-color" value="{{ $variant->color->name }}" disabled>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span3">
                                            <label for="product-condition">Condition</label>
                                            <input type="text" name="product-condition" value="{{ $variant->condition->name }}" disabled>
                                        </div>
                                        <div class="span3">
                                            <label for="product-weight">Weight <span class="hint">(lb)</span></label>
                                            <input type="text" id="product-weight" name="product-weight" value="{{ $variant->weight }}" disabled>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <!-- Photos -->


                            <!-- Visibility -->


                        </form>
                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')










        <!-- Modals -->
        <div id="product-delete-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Delete {{ $variant->product->name }}?</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete <strong>{{ $variant->product->name }}</strong>? This is a permanent action and cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                <a href="#" id="confirm-product-delete" class="btn btn-danger">Delete</a>
            </div>
        </div>










        <!-- Javascript -->
        @include('admin.partials.js')










    </body>
</html>
