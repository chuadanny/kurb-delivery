<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- CSS -->
        @include('admin.partials.css')


    </head>
    <body>

        <!-- Start Wrap -->
        <div id="wrap">
            @include('admin.partials.header')


            <!-- Start Main -->
            <div class="main-wrapper clearfix">

                @include('admin.partials.sidebar')

                <!-- Start Content -->
                <div class="content-wrapper">
                    <div class="heading-row">
                        <h1>Customers</h1>
                        <div class="heading-toolbar">
                        </div>
                    </div>


                                
                    <div class="content">

                        <div class="toolbar">
                            <form id="filter-form" method="GET" action="/admin/orders">
                                <div class="btn-group search-box">
                                    <input class="span8 search-box" id="queryString" name="query" type="text" placeholder="Start typing a customer's name or location" value="{{ Input::get('query') }}">
                                </div>
                            </form>
                        </div>

                        <table class="table inventory-table" id="orders-table">
                            <thead>
                                <tr>
                                    <th class="select"></th>
                                    <th class="is-sortable">Name</th>
                                    <th class=" is-sortable">Location</th>
                                    <th class=" is-sortable">Orders Made</th>
                                    <th class=" is-sortable">Products Ordered</th>
                                    <th class=" is-sortable">Total Spent</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($customers))
                                    @foreach($customers as $customer_id => $customer)
                                        <tr>
                                            <td><input type="checkbox"></input></td>
                                            <td>
                                                <a href="/admin/customer/{{ $customer_id }}">
                                                    {{ $customer->first_name }} {{ $customer->last_name }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $customer->city }}, {{ strtoupper($customer->state) }}, {{ strtoupper($customer->country) }}
                                            </td>
                                            <td>
                                                {{ $customer->ordersCountInShop($shop->id) }}
                                            </td>
                                            <td>
                                                {{ $customer->productOrderedCountInShop($shop->id) }}
                                            </td>
                                            <td>${{ $customer->totalSpentInShop($shop->id) }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>


                        @if(isset($variants))
                            {{ $variants->appends($oldInput)->links() }}
                        @endif

                        

                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- End Main -->            
        </div>
        <!-- End Wrap -->


        @include('admin.partials.footer')





        <!-- Javascript -->
        @include('admin.partials.js')









    </body>
</html>
